--> Totals
WITH favorites AS (
  SELECT
    f.*,
    mf.profile_id AS mutually_favorited_by,
    CASE
      WHEN mutually_favorited_by is NULL OR mf.id > f.id THEN 'Favorite'
      WHEN mutually_favorited_by is NOT NULL THEN 'Mutual_Favorite'
    END AS favorite_type
  FROM seeking_arrange.favorites AS f
  LEFT JOIN seeking_arrange.favorites AS mf ON f.profile_id = mf.member_id
    AND f.member_id = mf.profile_id
    AND f.deleted_at IS NULL
  WHERE 1=1
    AND f.created_at >= '2022-06-01T00:00:00.000+00:00'
)
SELECT 
  favorite_type,
  COUNT(favorite_type) AS count
FROM favorites
GROUP BY 1

--> Monthly
WITH favorites AS (
  SELECT
    f.*,
    mf.profile_id AS mutually_favorited_by,
    CASE
      WHEN mutually_favorited_by is NULL OR mf.id > f.id THEN 'Favorite'
      WHEN mutually_favorited_by is NOT NULL THEN 'Mutual_Favorite'
    END AS favorite_type
  FROM seeking_arrange.favorites AS f
  LEFT JOIN seeking_arrange.favorites AS mf ON f.profile_id = mf.member_id
    AND f.member_id = mf.profile_id
    AND f.deleted_at IS NULL
  WHERE 1=1
    AND f.created_at >= '2022-06-01T00:00:00.000+00:00'
)
SELECT
  DATE_PART(month,created_at::timestamp) AS created_month,
  favorite_type,
  COUNT(favorite_type) AS count
FROM favorites
GROUP BY 1,2

--> Test case example
SELECT 
  f.*,
  mf.profile_id AS mutually_favorited_by,
  CASE
    WHEN mutually_favorited_by is NULL OR mf.id > f.id THEN 'Favorite'
    WHEN mutually_favorited_by is NOT NULL THEN 'Mutual_Favorite'
  END AS favorite_type
FROM seeking_arrange.favorites AS f
LEFT JOIN seeking_arrange.favorites AS mf ON f.profile_id = mf.member_id
  AND f.member_id = mf.profile_id
  AND f.deleted_at IS NULL
WHERE 1=1
  --> Personal test account
  AND f.profile_id = 40279021
  AND f.member_id = 19089239
  AND f.created_at >= '2022-11-01T00:00:00.000+00:00'
UNION
SELECT 
  f.*,
  mf.profile_id AS mutually_favorited_by,
  CASE
    WHEN mutually_favorited_by is NULL OR mf.id > f.id THEN 'Favorite'
    WHEN mutually_favorited_by is NOT NULL THEN 'Mutual_Favorite'
  END AS favorite_type
FROM seeking_arrange.favorites AS f
LEFT JOIN seeking_arrange.favorites AS mf ON f.profile_id = mf.member_id
  AND f.member_id = mf.profile_id
  AND f.deleted_at IS NULL
WHERE 1=1
  --> Personal test account
  AND f.profile_id = 19089239
  AND f.member_id = 40279021
  AND f.created_at >= '2022-11-01T00:00:00.000+00:00'

--> Distinct senders and total favorites (general)
SELECT
  COUNT(id) AS total_fav_count,
  COUNT(DISTINCT profile_id) AS distinct_senders
FROM seeking_arrange.favorites
WHERE 1=1
  AND created_at >= '2022-11-01T00:00:00.000+00:00'