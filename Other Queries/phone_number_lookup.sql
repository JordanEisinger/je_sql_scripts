-- SA messages phone number lookup
SELECT
  m.id as message_id,
  m.profile_id as sender,
  m.body as message,
  m.created_at,
  TRANSLATE(
    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LOWER(m.body), 'zero', '0'), 'one', '1'), 'two', '2'), 'three', '3'), 
    'four', '4'), 'five', '5'), 'six', '6'), 'seven', '7'), 'eight', '8'), 'nine', '9'),
    ' !"#$%&''()*+,-./:;<=>?@[\\]^_`{|}~', ''
  ) AS translated_message,
  REGEXP_SUBSTR(translated_message, '\\d{9,13}') AS matched_number
FROM seeking_arrange.messages as m
WHERE 1=1
  AND translated_message ilike '%5709122808%'

-- SA payments phone number lookup
SELECT
  t.id,
  t.customer_id,
  JSON_EXTRACT_PATH_TEXT(t.attributes,'payment_token','card','phone',true) as phone
FROM (
    SELECT 
        pt.id,
        pt.customer_id,
        udf.decr(pt.extended_attributes, 'SA') as attributes
    FROM seeking_arrange.paymenttokens AS pt
) AS t
WHERE 1=1
  AND phone ilike '%5709122808%'

-- U2P Payments phone number lookup
SELECT
  t.id,
  t.customer_id,
  JSON_EXTRACT_PATH_TEXT(t.attributes,'phone_number',true) as phone
FROM (
    SELECT 
        pt.id,
        pt.customer_id,
        udf.decr(pt.auth_data, 'U2P') as attributes
    FROM u2p_sites.paymenttokens AS pt
) AS t
WHERE 1=1
  AND phone ilike '%5709122808%'

-- WYP Payments phone number lookup
SELECT 
  pt.id,
  pt.account_id,
  udf.decr(pt.phone, 'WYP') as phone
FROM wyp.paymenttokens AS pt
WHERE 1=1
  AND phone ilike '%5709122808%'

--> Script for phone number search in WYP messages that returns decrypted sender and recipient emails
--> Not the cleanest so can be improved
SELECT
  message_id,
  sender,
  sender_email,
  recipient,
  recipient_email,
  message,
  created_dt,
  read_dt
FROM(
SELECT
  m.id as message_id,
  m.account_id as sender,
  UDF.DECR(a_1.v3_email_crypt, 'WYP') as sender_email,
  m.to_account_id as recipient,
  UDF.DECR(a_2.v3_email_crypt, 'WYP') as recipient_email,
  m.thread_id,
  m.message,
  m.created_dt,
  m.read_dt,
  TRANSLATE(
    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LOWER(m.message), 'zero', '0'), 'one', '1'), 'two', '2'), 'three', '3'), 'four', '4'), 'five', '5'), 'six', '6'), 'seven', '7'), 'eight', '8'), 'nine', '9'),
    ' !"#$%&''()*+,-./:;<=>?@[\\]^_`{|}~', ''
  ) AS translated_message,
  REGEXP_SUBSTR(translated_message, '\\d{9,13}') AS matched_number
FROM wyp.messages as m
LEFT JOIN wyp.accounts as a_1 ON a_1.id = m.account_id
LEFT JOIN wyp.accounts as a_2 ON a_2.id = m.to_account_id
WHERE 1=1
  AND translated_message ilike '%7088031011%'
)