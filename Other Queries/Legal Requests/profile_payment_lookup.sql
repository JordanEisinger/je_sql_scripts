/* ===================================================================================
As of: 11/03/2022

General Notes:
-Initially created as part of DST-2469
-Returns all U2P payment records given a Seeking profile id

Changelog:
11/03/22: Created initial query
11/09/22: Added query for payment lookup by cardholder name
=================================================================================== */

--> U2P Payment lookup for specific profile of interest when profile_id, uid, and/or user_id are known
WITH seeking_profiles AS (
  SELECT id, uid, user_id
  FROM seeking_arrange.profiles
  WHERE 1=1
    --> Insert Seeking uid
    AND uid = 'f378f714-41e0-42bc-a665-139f1a965751'
    --> Insert Seeking profile id
    AND id = 29651116
    --> Insert Seeking userr id
--     AND user_idd = '###'
),
--> Needed to get customer_id to link Seeking to U2P
u2p_customers AS (
  SELECT
    c.id AS customer_id, c.account_id
  FROM u2p_sites.customers AS c
  --> Note: u2p_sites.account_id = seeking_arrange.profiles.user_id
  INNER JOIN seeking_profiles AS sp ON sp.user_id = c.account_id
)
SELECT p.* 
FROM u2p_sites.payments AS p
INNER JOIN u2p_customers AS c ON c.customer_id = p.customer_id
ORDER BY p.id DESC


--> U2P Payment lookup for specific profiles of interest when only card name is known
WITH u2p_paymenttokens AS (
  SELECT
    id AS paymenttoken_id, 
    customer_id, 
    token AS customer_token, 
    customer_card_name
  FROM (
    SELECT
      *,
      udf.decr(pt.auth_data, 'U2P') AS decr_auth_data,
      JSON_EXTRACT_PATH_TEXT(decr_auth_data,'name',true) AS customer_card_name
    FROM u2p_sites.paymenttokens AS pt
    WHERE 1=1
      --> Insert name on card used (case-insensitive search)
      AND LOWER(customer_card_name) ilike '%maria%merino%'
  )
)
SELECT
  p.id AS payment_id, p.customer_id, pt.customer_card_name, pt.customer_token, p.site_id, p.transaction_reference
FROM u2p_sites.payments AS p
INNER JOIN u2p_paymenttokens AS pt ON pt.customer_id = p.customer_id