/* ===================================================================================
As of: 11/03/2022

General Notes:
-Scripts for deleted user lookup requests by email for SA and WYP
-Need to run in SQL Workbench b/c Looker can't run 'udf.decr' function
-Average total run time: ~30 minutes to 1 hour, depening on number of emails in WHERE clause
   which can backup Redshift replication, so run queries in batches by years
-Have AWS Alert dashboards pulled up to see if query is slowing replication

Changelog:
11/03/22: Added fuzzy search query
=================================================================================== */

--> Search for email username in SA using fuzzy match
SELECT
  p.id AS profile_id,
  p.uid AS uid,
  p.user_id AS user_id,
  udf.decr(u.email, 'SA') AS email,
  p.is_deleted
FROM seeking_arrange.users u
INNER JOIN seeking_arrange.profiles p ON p.user_id = u.id AND p.is_deleted >= 0
WHERE 1=1
/* Batch by years */
  AND u.created_at >= '2022-01-01'
--   AND u.created_at BETWEEN '2021-01-01' AND '2021-12-31'
--   AND u.created_at BETWEEN '2020-01-01' AND '2020-12-31'
--   AND u.created_at BETWEEN '2019-01-01' AND '2019-12-31'
--   AND u.created_at BETWEEN '2018-01-01' AND '2018-12-31'
--   AND u.created_at BETWEEN '2017-01-01' AND '2017-12-31' 
--   AND u.created_at BETWEEN '2016-01-01' AND '2016-12-31' 
--   AND u.created_at BETWEEN '2007-01-01' AND '2015-12-31'
--> Email usernames need to be lowercase to match lower() function
  AND (
    split_part(lower(udf.decr(email, 'SA')::varchar(384)), '@', 1) ilike '%insert%here%' 
    OR split_part(lower(udf.decr(email, 'SA')::varchar(384)), '@', 1) ilike '%insert%here%' 
      )
;

--> Search for email username in SA using exact match
SELECT
  p.id AS profile_id,
  p.uid AS uid,
  p.user_id AS user_id,
  udf.decr(u.email, 'SA') AS email,
  p.is_deleted
FROM seekINg_arrange.users u
INNER JOIN seekINg_arrange.profiles p ON p.user_id = u.id AND p.is_deleted > 0
WHERE 1=1
/* Batch by years */
  AND u.created_at >= '2022-01-01'
--   AND u.created_at BETWEEN '2021-01-01' AND '2021-12-31'
--   AND u.created_at BETWEEN '2020-01-01' AND '2020-12-31'
--   AND u.created_at BETWEEN '2019-01-01' AND '2019-12-31'
--   AND u.created_at BETWEEN '2018-01-01' AND '2018-12-31'
--   AND u.created_at BETWEEN '2017-01-01' AND '2017-12-31'
--   AND u.created_at BETWEEN '2016-01-01' AND '2016-12-31'
--   AND u.created_at BETWEEN '2007-01-01' AND '2015-12-31'
  AND split_part(lower(udf.decr(email, 'SA')::varchar(384)), '@', 1) IN 
/* Email usernames need to be lowercase to match lower() function */
    (
      'inserthere'
    )
;

--> Search for email domains in SA using fuzzy match
SELECT
  p.id AS profile_id,
  p.uid AS uid,
  p.user_id AS user_id,
  udf.decr(u.email, 'SA') AS email,
  p.is_deleted
FROM seeking_arrange.users u
INNER JOIN seekINg_arrange.profiles p ON p.user_id = u.id AND p.is_deleted > 0
WHERE 1=1
/* Batch by years */
--   AND u.created_at >= '2022-01-01' -- No Results
--   AND u.created_at BETWEEN '2021-01-01' AND '2021-12-31' -- No Results
--   AND u.created_at BETWEEN '2020-01-01' AND '2020-12-31' -- No Results
--   AND u.created_at BETWEEN '2019-01-01' AND '2019-12-31' -- No Results
--   AND u.created_at BETWEEN '2018-01-01' AND '2018-12-31' -- No Results
--   AND u.created_at BETWEEN '2017-01-01' AND '2017-12-31' -- No Results
--   AND u.created_at BETWEEN '2016-01-01' AND '2016-12-31' -- No Results
--   AND u.created_at BETWEEN '2007-01-01' AND '2015-12-31' -- No Results
--> Email domains need to be lowercase to match lower() function
  AND (
    split_part(lower(udf.decr(email, 'SA')::varchar(384)), '@', 2) ilike '%insert%here%' 
    OR split_part(lower(udf.decr(email, 'SA')::varchar(384)), '@', 2) ilike '%insert%here%' 
      )
;

--> Search for email domains in SA using exact match
SELECT
  p.id AS profile_id,
  p.uid AS uid,
  p.user_id AS user_id,
  udf.decr(u.email, 'SA') AS email,
  p.is_deleted
FROM seeking_arrange.users u
INNER JOIN seekINg_arrange.profiles p ON p.user_id = u.id AND p.is_deleted > 0
WHERE 1=1
/* Batch by years */
--   AND u.created_at >= '2022-01-01' -- No Results
--   AND u.created_at BETWEEN '2021-01-01' AND '2021-12-31' -- No Results
--   AND u.created_at BETWEEN '2020-01-01' AND '2020-12-31' -- No Results
--   AND u.created_at BETWEEN '2019-01-01' AND '2019-12-31' -- No Results
--   AND u.created_at BETWEEN '2018-01-01' AND '2018-12-31' -- No Results
--   AND u.created_at BETWEEN '2017-01-01' AND '2017-12-31' -- No Results
--   AND u.created_at BETWEEN '2016-01-01' AND '2016-12-31' -- No Results
--   AND u.created_at BETWEEN '2007-01-01' AND '2015-12-31' -- No Results
  AND split_part(lower(udf.decr(email, 'SA')::varchar(384)), '@', 2) IN 
/* email domains need to be lowercase to match lower() function */
    (
      'inserthere'
    )
;

--> Script for deleted user lookip for WYP
--> GDPR deleted users have a 5 character string appended on end of email, so use SUBSTRING to remove
SELECT
  id,
  udf.decr(v3_email_crypt, 'WYP') as decr_email,
  --> Substring only needed for GDPR deleted acccounts
  SUBSTRING(decr_email,1,LEN(decr_email) -5) as email,
  created_dt
FROM (
  SELECT
    a.id,
    a.v3_email_crypt,
    a.deleted AS is_deleted,
    a.created_dt
  FROM wyp.accounts AS a
  WHERE 1=1
    AND deleted = TRUE 
--> Two filters are for current day deleted and GDPR deleted
    -- AND deleted_dt < GETDATE() - INTERVAL '1 day'
    -- AND deleted_reason = 'gdpr'
)
WHERE 1=1
/* Batch by years */
--   AND created_dt >= '2022-01-01' -- No Results
--   AND created_dt BETWEEN '2016-01-01' AND '2021-12-31' -- No Results
--   AND created_dt BETWEEN '2011-03-23' AND '2015-12-31' -- No Results
  AND SPLIT_PART(LOWER(udf.decr(v3_email_crypt, 'WYP')::varchar(384)), '@', 1) IN 
/* Needs to be lowercase */
    (
      'inserthere'
    )
;