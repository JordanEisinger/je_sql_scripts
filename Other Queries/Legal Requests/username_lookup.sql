/* ===================================================================================
As of: 02/13/2023 

General Notes:
- Username records are stored in multiple places, so there isn't one single source for query
- Usernames are also non-unique, meaning multiple users can have the same username
- PAS tables are closest to username changelog

Changelog:
07/29/2022: Re-organized query order, put PAS queries first
11/21/2022: Consolidated redundant queries
02/13/2023: Updated username looukup to account for multiple usernames
=================================================================================== */

--> Any User Username lookup in PAS contents table
SELECT 
  pc.id,
  pp.customer_id AS user_id,
  pp.profile_key AS profile_uid,
  pc.pasprofile_id,
  JSON_EXTRACT_PATH_TEXT(pc.content_data, 'data', 'text', 0, 'value', true) AS username,
  pc.created_at,
  pc.updated_at,
  pc.deleted_at
FROM seeking_arrange.pascontents AS pc
LEFT JOIN seeking_arrange.pasprofiles AS pp ON pp.id = pc.pasprofile_id
WHERE 1=1
  --> Filter on only username labels
  AND JSON_EXTRACT_PATH_TEXT(pc.content_data, 'data', 'text', 0, 'label', true) = 'Username'
  --> Lookup by username
  AND (
    LOWER(username) ilike '%ashmay%23%'
    OR LOWER(username) ilike '%cali%babe&23%'
    OR LOWER(username) ilike '%1%good&daddy%' 
  )
ORDER BY pasprofile_id ASC 

--> Old Accounts Username lookup in SA approvals table
SELECT
  id AS approvals_id,
  createdby_profile_id AS profile_id,
  attribute_value AS username
FROM seeking_arrange.approvals 
WHERE 1=1
  --> username id
  AND attribute_id = 23
  --> Lookup by username
  AND (
    LOWER(username) ilike '%ashmay%23%'
    OR LOWER(username) ilike '%cali%babe&23%'
    OR LOWER(username) ilike '%1%good&daddy%' 
  )

--> Current Users Username lookup in SA profile field vales table
SELECT
  id AS profilefieldvalues_id,
  profile_id,
  value AS username,
  created_at,
  updated_at 
FROM seeking_arrange.profilefieldvalues
WHERE 1=1
  --> username_id
  AND profileattribute_id = 23 
  --> Lookup by username
  AND (
    LOWER(username) ilike '%ashmay%23%'
    OR LOWER(username) ilike '%cali%babe&23%'
    OR LOWER(username) ilike '%1%good&daddy%' 
  )

  



