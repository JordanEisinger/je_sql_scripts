/* ===================================================================================
As of: 11/01/2022

General Notes:
-The results of the exact match filter is included in keyword match filer;
  only separated for distinguishing between the two in results

Changelog:
11/01/22: Updated WHERE logic to include OR statements; changed file name and moved 
          to new folder
=================================================================================== */

SELECT * FROM seeking_arrange.reports 
WHERE 1=1
/* Exact Matches */
  AND LOWER(description) ilike '%successfulmatch.com%'
  OR LOWER(description) ilike '%sugardaddymeet.com%'
  OR LOWER(description) ilike '%seekingmillionaire.com%'
/* Keyword Matches */
  AND LOWER(description) ilike '%successful%match%'
  OR LOWER(description) ilike '%sugardaddymeet%'
  OR LOWER(description) ilike '%seekingmillionaire%' 