-- Only distinct part of endpoint is last part after final '/'
SELECT distinct endpoint FROM seeking_arrange.apilogs LIMIT 100

-- Reverses, gets last part of string, then reverses back to normal
SELECT
  endpoint,
  REVERSE(SPLIT_PART(REVERSE(endpoint),'/',1))
FROM seeking_arrange.apilogs
WHERE endpoint <> ''
LIMIT 10

-- Frequency of last part of string
SELECT
  REVERSE(SPLIT_PART(REVERSE(endpoint),'/',1)) as string,
  COUNT(string)
FROM seeking_arrange.apilogs
WHERE endpoint <> ''
GROUP BY 1
ORDER BY 2 DESC
LIMIT 100