/* =================================================================================== */
/* NOTES */
/* Last Updated At: 02/23/2022
/*
/* Queries pull all thread records for a noted profile of interest and a full list
   of profiles they interacted with 
/* =================================================================================== */

-- Query 1
-- Pulls data from PAS contents tables
WITH threads_of_interest AS (
  SELECT
    profile_id_2 AS profile_of_interest,
    created_at AS thread_created_at,
    last_message_from_me_at_1 AS most_recent_message_sent_by_jg,
    last_message_from_me_at_2 AS most_recent_message_sent_by_other
  FROM seeking_arrange.threads
  WHERE profile_id_1  = 2663956
    AND created_at < '2018-01-01'
  UNION
  SELECT  
    profile_id_1 AS profile_of_interest,
    created_at AS thread_created_at,
    last_message_from_me_at_2 AS most_recent_message_sent_by_jg,
    last_message_from_me_at_1 AS most_recent_message_sent_by_other
  FROM seeking_arrange.threads
  WHERE profile_id_2 = 2663956
    AND created_at < '2018-01-01'
),
profile_list AS (
  SELECT p.id AS profile_id, p.uid AS profile_uid, pp.id AS pasprofile_id
  FROM seeking_arrange.profiles AS p
  INNER JOIN threads_of_interest AS t ON t.profile_of_interest = p.id
  LEFT JOIN seeking_arrange.pasprofiles AS pp ON pp.profile_key = p.uid
)
SELECT 
  pl.*,
  pc.id AS pascontent_id, pc.pasrequest_id, pct.name, 
  CASE 
    WHEN pct.name IN ('Profile', 'Photo', 'Escalated Profile', 'New User Photo') 
    THEN CASE WHEN JSON_EXTRACT_PATH_TEXT(content_data, 'data','image',0,'privacy',true) = 'private' THEN 'private' 
    ELSE NULLIF(JSON_EXTRACT_PATH_TEXT(content_data, 'data','image',0, 'url_large', true), '')
    END
  END AS photo_url,
  CASE 
    WHEN pct.name IN('Profile', 'Text', 'Escalated Profile', 'New User Text') THEN NULLIF(JSON_EXTRACT_PATH_TEXT(content_data, 'data', 'text', 0, 'label', true), '')
  END AS text_type,
  CASE 
    WHEN pct.name IN('Profile', 'Text', 'Escalated Profile', 'New User Text') THEN NULLIF(JSON_EXTRACT_PATH_TEXT(content_data, 'data', 'text', 0, 'value', true), '') 
  END AS text_content,
  CASE 
    WHEN pct.name = 'Auto-Flagged Messages' THEN NULLIF(JSON_EXTRACT_PATH_TEXT(content_data, 'data', 'message', 0, 'value', true), '') 
  END AS auto_flagged_message,
  NULLIF(pc.extended_data, '""') AS extended_data,
  pc.moderator, pc.moderated_at, pc.created_at, pc.updated_at, pc.deleted_at
FROM profile_list AS pl
LEFT JOIN seeking_arrange.pascontents AS pc ON pc.pasprofile_id = pl.pasprofile_id
LEFT JOIN seeking_arrange.pascontenttypes AS pct ON pct.id = pc.pascontenttype_id
WHERE 1=1
  AND profile_id = 7596854
ORDER BY pl.profile_id, pc.id



-- Query 2
-- Same logic as Query 1, but only pulls from PAS moderations and PAS requests table
WITH threads_of_interest AS (
  SELECT
    profile_id_2 AS profile_of_interest,
    created_at AS thread_created_at,
    last_message_from_me_at_1 AS most_recent_message_sent_by_jg,
    last_message_from_me_at_2 AS most_recent_message_sent_by_other
  FROM seeking_arrange.threads
  WHERE profile_id_1  = 2663956
    AND created_at < '2018-01-01'
  UNION
  SELECT  
    profile_id_1 AS profile_of_interest,
    created_at AS thread_created_at,
    last_message_from_me_at_2 AS most_recent_message_sent_by_jg,
    last_message_from_me_at_1 AS most_recent_message_sent_by_other
  FROM seeking_arrange.threads
  WHERE profile_id_2 = 2663956
    AND created_at < '2018-01-01'
),
profile_list AS (
  SELECT p.id AS profile_id, p.uid AS profile_uid, pp.id AS pasprofile_id
  FROM seeking_arrange.profiles AS p
  INNER JOIN threads_of_interest AS t ON t.profile_of_interest = p.id
  LEFT JOIN seeking_arrange.pasprofiles AS pp ON pp.profile_key = p.uid
)
SELECT 
  pl.*,
  pm.id AS pasmoderation_id, 
  pm.pasmoderator_id,
  pm.pascontent_id,
  pm.pasrequest_id,
  JSON_EXTRACT_PATH_TEXT(pm.extended_data, 'data', 0, 'label', true) AS text_label,
  JSON_EXTRACT_PATH_TEXT(pm.extended_data, 'data', 0, 'old_value', true) AS text_old_value,
  JSON_EXTRACT_PATH_TEXT(pm.extended_data, 'data', 0, 'new_value', true) AS text_new_value,
  pm.status,
  pm.created_at,
  pm.updated_at
FROM profile_list AS pl
LEFT JOIN seeking_arrange.pasrequests AS pr ON pr.pasprofile_id = pl.pasprofile_id
LEFT JOIN seeking_arrange.pasmoderations AS pm ON pm.pasrequest_id = pr.id
WHERE 1=1
  AND pm.type <> 'photo'
  AND profile_id = 7596854
ORDER BY pl.profile_id, pr.id



