/* ===================================================================================
As of: 02/24/2022

General Notes:
-Initially created for DST-1942
-Search for specific email domains, including other filters

Changelog:
-After testing, query runs faster if SPLIT_PART() is performed in WHERE clause as 
  opposed to in SELECT statement
=================================================================================== */

SELECT
  u.id AS user_id,
  p.id AS profile_id,
  p.uid AS profile_uid,
  udf.decr(email, 'SA')::varchar(384) AS email_1,
  p.is_fraud,
  p.is_suspended,
  u.is_fraud_deleted,
  p.is_deleted AS is_deleted_by_user,
  u.created_at,
  p.deleted_at
FROM seeking_arrange.users AS u 
LEFT JOIN seeking_arrange.profiles AS p ON p.user_id = u.id
WHERE 1=1
  AND u.created_at >= '2021-11-01 00:00:00'
  AND split_part(email_1,'@',2) ilike '%campus.mccd.edu%'
  AND u.is_fraud_deleted = 1
ORDER BY u.created_at ASC