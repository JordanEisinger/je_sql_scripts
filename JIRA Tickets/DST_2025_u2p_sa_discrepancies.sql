/* ===================================================================================
As of: 05/02/2022

General Notes:
-seeking_arrange.payments.transaction_seq <=> u2p_sites.payments.id

Changelog:
05/02/22: Most of the discrepancies seem to come from chargeback transations, 
          specifically U2P reporting a value and SA reporting no amount
=================================================================================== */

--> Starting with U2P's transactions and comparing to SA -->
SELECT * FROM (
  SELECT
    true AS u2p_to_sa_perspective,
    p.id AS u2p_payment_id,
    sp.id AS sa_payment_id,
    pl.u2p_paymentlogs_transaction_references AS u2p_tran_refs,
    spl.sa_paymentlogs_transaction_references AS sa_tran_refs,
    p.currency AS u2p_currency,
    sp.currency AS sa_currency,
    p.status AS u2p_status,
    sp.status AS sa_status,
    pl.total_u2p_paymentlogs_amount AS total_u2p_amount,
    spl.total_sa_paymentlogs_amount AS total_sa_amount,
    s.name AS site_name,
    p.created_at::timestamp AS u2p_created_at,
    spl.min_sa_payment_created_at,
    spl.max_sa_payment_created_at,
    ABS(COALESCE(pl.total_u2p_paymentlogs_amount, 0) - COALESCE(spl.total_sa_paymentlogs_amount, 0)) AS u2p_vs_sa_discrepancy
  FROM u2p_sites.payments AS p
  --> Get SA payments -->
  LEFT JOIN seeking_arrange.payments AS sp ON sp.transaction_seq = p.id
  --> Get Site names -->
  LEFT JOIN u2p_core.sites AS s ON s.id = p.site_id
  --> Get U2P paymentlog summed amount -->
  LEFT JOIN (
    SELECT
      payment_id,
      SUM(amount) AS total_u2p_paymentlogs_amount,
      LISTAGG(DISTINCT transaction_reference, ',') AS u2p_paymentlogs_transaction_references,
      MIN(created_at)::timestamp AS min_u2p_payment_created_at,
      MAX(created_at)::timestamp AS max_u2p_payment_created_at
    FROM u2p_sites.paymentlogs
    WHERE 1=1
      AND status <> 'pending'
    GROUP BY payment_id
   ) AS pl ON pl.payment_id = p.id
  --> Get SA paymentlog summed amount ->
  LEFT JOIN (
    SELECT
      payment_id,
      SUM(amount) AS total_sa_paymentlogs_amount,
      LISTAGG(DISTINCT transaction_reference, ',') AS sa_paymentlogs_transaction_references,
      MIN(created_at)::timestamp AS min_sa_payment_created_at,
      MAX(created_at)::timestamp AS max_sa_payment_created_at
    FROM seeking_arrange.paymentlogs
    WHERE 1=1
      AND status <> 'pending'
    GROUP BY payment_id
   ) AS spl ON spl.payment_id = sp.id
WHERE 1=1
  --> Only SA payments in U2P -->
  AND s.name = 'SeekingArrangement'
ORDER BY u2p_created_at DESC
-- LIMIT 10000
) AS u1

UNION

--> Starting with SA's transactions and comparing to U2P -->
SELECT * FROM (
  SELECT
    false AS u2p_to_sa_perspective,
    p.id AS u2p_payment_id,
    sp.id AS sa_payment_id,
    pl.u2p_paymentlogs_transaction_references AS u2p_tran_refs,
    spl.sa_paymentlogs_transaction_references AS sa_tran_refs,
    p.currency AS u2p_currency,
    sp.currency AS sa_currency,
    p.status AS u2p_status,
    sp.status AS sa_status,
    pl.total_u2p_paymentlogs_amount AS total_u2p_amount,
    spl.total_sa_paymentlogs_amount AS total_sa_amount,
    s.name AS site_name,
    p.created_at::timestamp AS u2p_created_at,
    spl.min_sa_payment_created_at,
    spl.max_sa_payment_created_at,
    ABS(COALESCE(pl.total_u2p_paymentlogs_amount, 0) - COALESCE(spl.total_sa_paymentlogs_amount, 0)) AS u2p_vs_sa_discrepancy
  FROM seeking_arrange.payments AS sp
  --> Get U2P payments -->
  LEFT JOIN u2p_sites.payments AS p ON p.id = sp.transaction_seq
  --> Get Site names -->
  LEFT JOIN u2p_core.sites AS s ON s.id = p.site_id
  --> Get SA paymentlog summed amount ->
  LEFT JOIN (
    SELECT
      payment_id,
      SUM(amount) AS total_sa_paymentlogs_amount,
      LISTAGG(DISTINCT transaction_reference, ',') AS sa_paymentlogs_transaction_references,
      MIN(created_at)::timestamp AS min_sa_payment_created_at,
      MAX(created_at)::timestamp AS max_sa_payment_created_at
    FROM seeking_arrange.paymentlogs
    WHERE 1=1
      AND status <> 'pending'
    GROUP BY payment_id
   ) AS spl ON spl.payment_id = sp.id
  --> Get U2P paymentlog summed amount -->
  LEFT JOIN (
    SELECT
      payment_id,
      SUM(amount) AS total_u2p_paymentlogs_amount,
      LISTAGG(DISTINCT transaction_reference, ',') AS u2p_paymentlogs_transaction_references,
      MIN(created_at)::timestamp AS min_u2p_payment_created_at,
      MAX(created_at)::timestamp AS max_u2p_payment_created_at
    FROM u2p_sites.paymentlogs
    WHERE 1=1
      AND status <> 'pending'
    GROUP BY payment_id
   ) AS pl ON pl.payment_id = p.id
ORDER BY spl.max_sa_payment_created_at DESC
-- LIMIT 10000
) AS u2