SELECT * 
FROM seeking_arrange.adminnotes 
WHERE 1=1
  AND note ilike '%spam v%'
LIMIT 100

SELECT * FROM seeking_arrange.frauddeletes ORDER BY created_at DESC LIMIT 10 
SELECT * FROM seeking_arrange.frauddeletereasons ORDER BY id ASC LIMIT 50
SELECT * FROM seeking_arrange.frauddeletemodules ORDER BY id ASC LIMIT 50

-- CTE's
WITH marked_as_spam_list AS (
SELECT
  a.id AS adminnotes_id,
  a.user_id AS adminnotes_user_id,
  a.profile_id AS adminnotes_profile_id,
  a.note,
  a.created_at AS adminnotes_created_at,
  fd.id AS frauddelete_id,
  fd.member_profile_id AS frauddelete_profile_id,
  fd.fraud_delete_reason_id,
  fdr.name AS fraud_delete_reason,
  fdm.name AS fraud_delete_module,
  fd.created_at AS frauddelete_created_at
FROM seeking_arrange.adminnotes AS a
LEFT JOIN seeking_arrange.frauddeletes AS fd ON fd.member_profile_id = a.profile_id
LEFT JOIN seeking_arrange.frauddeletereasons AS fdr ON fdr.id = fd.fraud_delete_reason_id
LEFT JOIN seeking_arrange.frauddeletemodules AS fdm ON fdm.id = fd.frauddeletemodule_id
WHERE 1=1
  AND a.note = 'Marked as spammed by system'  
--   AND a.note = 'Profile Spam Verified'
  AND a.created_at >= '2022-01-01'
--   AND a.created_at BETWEEN '2022-02-13' AND '2022-02-14'
--   AND fdm.name = 'SPAM List'
LIMIT 500
),
spam_verified_list AS (
SELECT
  a.id AS adminnotes_id,
  a.user_id AS adminnotes_user_id,
  a.profile_id AS adminnotes_profile_id,
  a.note,
  a.created_at AS adminnotes_created_at,
  fd.id AS frauddelete_id,
  fd.member_profile_id AS frauddelete_profile_id,
  fd.fraud_delete_reason_id,
  fdr.name AS fraud_delete_reason,
  fdm.name AS fraud_delete_module,
  fd.created_at AS frauddelete_created_at
FROM seeking_arrange.adminnotes AS a
LEFT JOIN seeking_arrange.frauddeletes AS fd ON fd.member_profile_id = a.profile_id
LEFT JOIN seeking_arrange.frauddeletereasons AS fdr ON fdr.id = fd.fraud_delete_reason_id
LEFT JOIN seeking_arrange.frauddeletemodules AS fdm ON fdm.id = fd.frauddeletemodule_id
WHERE 1=1
--   AND a.note = 'Marked as spammed by system'  
  AND a.note = 'Profile Spam Verified'
  AND a.created_at >= '2022-01-01'
--   AND a.created_at BETWEEN '2022-02-13' AND '2022-02-14'
--   AND fdm.name = 'SPAM List'
LIMIT 1000
),
dates_list AS (
SELECT
  adminnotes_profile_id AS profile_id,
  adminnotes_created_at AS marked_as_spam_date,
  frauddelete_created_at AS fraud_delete_date,
  CONVERT(DECIMAL(12,2),CONVERT(DECIMAL(12,2),DATEDIFF(SECOND,adminnotes_created_at::timestamp,frauddelete_created_at::timestamp))/60/60/24) AS day_diff,
  CONVERT(DECIMAL(12,2),CONVERT(DECIMAL(12,2),DATEDIFF(SECOND,adminnotes_created_at::timestamp,frauddelete_created_at::timestamp))/60/60) AS hour_diff,
  CONVERT(DECIMAL(12,2),CONVERT(DECIMAL(12,2),DATEDIFF(SECOND,adminnotes_created_at::timestamp,frauddelete_created_at::timestamp))/60) AS minute_diff
FROM marked_as_spam_list
)
-- Notes:
-- Some profiles have a marked as spam date that is before fraud delete date, do difference is negative
-- Only want a single number for average metrics, but think it is more helpful for a daily number
-- SELECT 
--   TRUNC(marked_as_spam_date::timestamp) as marked_spam_date,
--   MAX(day_diff) AS max_day_diff,
--   MIN(day_diff) AS min_day_diff,
--   AVG(day_diff) AS avg_day_diff,
--   AVG(hour_diff) AS avg_hour_diff,
--   AVG(minute_diff) AS avg_min_diff
-- FROM list_2
-- GROUP BY 1
-- ORDER BY 1 DESC

SELECT * FROM spam_verified_list 
