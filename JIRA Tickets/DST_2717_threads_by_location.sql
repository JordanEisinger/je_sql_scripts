/* ===================================================================================
As of: 03/08/2023

General Notes:
- Initially for PR request of messages around specific time periods

Changelog:
03/08/2023: Added initial query to repo
=================================================================================== */

WITH profile_id_1_threads AS(
SELECT
  1234 AS id,
  COUNT(DISTINCT t.id) AS total_conversations
FROM seeking_arrange.threads AS t
INNER JOIN seeking_arrange.locations AS l ON l.profile_id = t.profile_id_1
WHERE 1=1
  AND t.created_at BETWEEN '2022-03-12T00:00:00.000+00:00' AND '2022-03-18T00:00:00.000+00:00'
  AND l.region ilike '%district of columbia%'
)
, profile_id_2_threads AS (
SELECT
  1234 AS id,
  COUNT(DISTINCT t.id) AS total_conversations
FROM seeking_arrange.threads AS t
INNER JOIN seeking_arrange.locations AS l ON l.profile_id = t.profile_id_2
WHERE 1=1
  AND t.created_at BETWEEN '2022-03-12T00:00:00.000+00:00' AND '2022-03-18T00:00:00.000+00:00'
  AND l.region ilike '%district of columbia%'
)
SELECT
  t1.total_conversations + t2.total_conversations AS total_us_conversations_march
FROM profile_id_1_threads AS t1
LEFT JOIN profile_id_2_threads AS t2 ON t2.id = t1.id 