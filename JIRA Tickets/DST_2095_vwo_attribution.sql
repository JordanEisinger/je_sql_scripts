--> VWO pop 41 & 43 in rollup table
--> Doesn't seem to have anything regarding revenue attribution
SELECT *
FROM rollup.vysion_event_flows 
WHERE 1=1
  AND (eventname ilike '%VWO_deploy_41%' OR eventname ilike '%VWO_deploy_43%')
  AND clientsessionid <> ''
ORDER BY clientsessionid
LIMIT 1000

--> Users who clicked VWO Deploy 41 or 43 and had billingSuccess in same session
SELECT
  vl.*
FROM vysion.vysion_logs AS vl
WHERE 1=1
  AND eventname = 'billingSuccess'
  AND clientsessionid IN (
    SELECT clientsessionid FROM vysion.vysion_logs
    WHERE 1=1
      AND ts_index BETWEEN '2022-05-01T00:00:00.000+00:00' AND '2022-06-08T00:00:00.000+00:00'
      AND (eventname ilike '%VWO_deploy_41%' OR eventname ilike '%VWO_deploy_43%')
  )
LIMIT 100

--> All events in an individual users session with VWO and billingSuccess
--> How to attribute billingSuccess to VWO 41 or 43 if another VWO campaign also in eventname list?
SELECT * FROM vysion.vysion_logs
WHERE 1=1
  AND ts_index BETWEEN '2022-05-01T00:00:00.000+00:00' AND '2022-06-08T00:00:00.000+00:00'
  -- AND (eventname ilike '%VWO_deploy_41%' OR eventname ilike '%VWO_deploy_43%')
  AND clientsessionid = '5cee447f-47b1-429d-be59-99f3b530a2d1'
ORDER BY server_ts ASC
LIMIT 200

--
SELECT
vl.*
  -- eventid, userid, clientsessionid, eventname, server_ts, userdata, devicedata, eventdata
  -- JSON_EXTRACT_PATH_TEXT(v1.eventdata,'digi',2,true) AS utm_medium,
  -- JSON_EXTRACT_PATH_TEXT(v1.eventdata,'digi',5,true) AS utm_campaign,
  -- JSON_EXTRACT_PATH_TEXT(v1.eventdata,'amount',true) as purchase_amt,
  -- COUNT(eventid) OVER (PARTITION BY clientsessionid) AS n_session_events
FROM vysion.vysion_logs AS vl
WHERE 1=1
  AND eventname = 'billingSuccess'
  AND clientsessionid IN (
    SELECT clientsessionid FROM vysion.vysion_logs
    WHERE 1=1
      AND ts_index BETWEEN '2022-05-01T00:00:00.000+00:00' AND '2022-06-16T00:00:00.000+00:00'
      AND (eventname ilike '%VWO_deploy_41%' OR eventname ilike '%VWO_deploy_43%')
    LIMIT 1000
  )
LIMIT 100

SELECT * FROM vysion.vysion_logs AS vl WHERE 1=1
      AND ts_index BETWEEN '2022-05-01T00:00:00.000+00:00' AND '2022-06-16T00:00:00.000+00:00' LIMIT 100
