/* ===================================================================================
As of: 07/05/2022

General Notes:
-List of accounts with id verification that fall under criteria for bug noted in ticket

Changelog:
=================================================================================== */

WITH idv AS (
  SELECT * 
  FROM seeking_arrange.verificationidentifications 
  WHERE 1=1
    AND (status = 'required' OR status = 'approved')
)
SELECT d.*
FROM seeking_arrange.dragynrequests AS d 
INNER JOIN idv ON idv.profile_id = d.profile_id
WHERE 1=1
  AND event = 'id_verification' AND (recommendation = 'INITIAL' OR recommendation = '')
ORDER BY d.id, d.profile_id DESC
-- LIMIT 100