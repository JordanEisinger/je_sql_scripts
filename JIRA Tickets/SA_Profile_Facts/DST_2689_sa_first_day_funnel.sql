/* ===================================================================================
As of: 03/17/2023

General Notes:
-Original query that includes profile attribute and field completion timestamps and
  first day funnel metrics

Changelog:
03/15/23: Added notes for context to query
03/16/23: Updated logic to pull from modified sa_profile_facts table, updated field names
  for readability
03/17/23: Updated logic for aggregaton
04/14/23: Re-wrote query as a rollup by day using sub-queries as oppposed to CTE's
05/08/23: Updated query to match LookML PDT
=================================================================================== */

SELECT
  --TRUNC(DATE_TRUNC('day',s.first_server_ts)) AS first_server_date
  TRUNC(DATE_TRUNC('day',CONVERT_TIMEZONE('UTC','America/Los_Angeles',s.first_server_ts))) AS first_server_date
  ,SUM(CASE WHEN s.clicked_anything_in_join_flow <> '' THEN 1 ELSE 0 END) AS total_sessions_started_join
  ,SUM(s.successfully_joined) AS total_sessions_finished_join
  ,SUM(CASE WHEN p.account_type = 'attractive' AND p.sex = 'Female' AND p.profile_attributes ILIKE '%"248"%' THEN 1 ELSE 0 END) AS af4m_joins
  ,SUM(CASE WHEN p.account_type = 'generous' AND p.sex = 'Male' AND p.profile_attributes ILIKE '%"249"%' THEN 1 ELSE 0 END) AS gm4f_joins
  ,SUM(
      CASE
        WHEN (
          CASE
            WHEN p.account_type = 'generous' AND p.sex = 'Male' AND p.profile_attributes ILIKE '%"249"%' THEN 'GM4F'
            WHEN p.account_type = 'attractive' AND p.sex = 'Female' AND p.profile_attributes ILIKE '%"248"%' THEN 'AF4M'
            ELSE
              CASE WHEN p.id IS NOT NULL THEN 'Other' END
          END
        ) = 'Other' THEN 1 ELSE 0 END
        ) AS other_joins
  ,AVG(CASE WHEN fdpcp.member_type = 'AF4M' THEN total_first_day_percent_complete ELSE NULL END) AS af4m_perc_compl
  ,AVG(CASE WHEN fdpcp.member_type = 'GM4F' THEN total_first_day_percent_complete ELSE NULL END) AS gm4f_perc_compl
  ,AVG(CASE WHEN fdpcp.member_type = 'Other' THEN total_first_day_percent_complete ELSE NULL END) AS other_perc_compl
  ,AVG(CASE WHEN ph.member_type = 'AF4M' THEN photo_count ELSE NULL END) AS af4m_photos
  ,AVG(CASE WHEN ph.member_type = 'GM4F' THEN photo_count ELSE NULL END) AS gm4f_photos
  ,AVG(CASE WHEN ph.member_type = 'Other' THEN photo_count ELSE NULL END) AS other_photos
  ,AVG(CASE WHEN pv.member_type = 'AF4M' THEN profile_view_count ELSE NULL END) AS af4m_profile_views
  ,AVG(CASE WHEN pv.member_type = 'GM4F' THEN profile_view_count ELSE NULL END) AS gm4f_profile_views
  ,AVG(CASE WHEN pv.member_type = 'Other' THEN profile_view_count ELSE NULL END) AS other_profile_views
  ,SUM(CASE WHEN py.member_type = 'GM4F' THEN memb_purchases ELSE NULL END) AS total_gm4f_memb_purch
  ,CAST(total_gm4f_memb_purch AS DECIMAL(10,1)) / CAST(gm4f_joins AS DECIMAL(10,1)) * 100 AS c2
  ,SUM(CASE WHEN r.member_type = 'GM4F' THEN day_0_revenue ELSE NULL END) AS total_d0_rev
  ,AVG(CASE WHEN r.member_type = 'GM4F' THEN day_0_revenue ELSE NULL END) AS arppu
  ,total_d0_rev / gm4f_joins AS arpu
  ,SUM(CASE WHEN m.member_type = 'AF4M' THEN first_day_message ELSE NULL END) AS af4m_first_day_message
  ,CAST(af4m_first_day_message AS DECIMAL(10,1)) / CAST(af4m_joins AS DECIMAL(10,1)) * 100 AS af4m_message_perc
  ,SUM(CASE WHEN m.member_type = 'GM4F' THEN first_day_message ELSE NULL END) AS gm4f_first_day_message
  ,CAST(gm4f_first_day_message AS DECIMAL(10,1)) / CAST(gm4f_joins AS DECIMAL(10,1)) * 100 AS gm4f_message_perc
  ,SUM(CASE WHEN m.member_type = 'Other' THEN first_day_message ELSE NULL END) AS other_first_day_message
  ,CAST(other_first_day_message AS DECIMAL(10,1)) / CAST(other_joins AS DECIMAL(10,1)) * 100 AS other_message_perc
  ,SUM(CASE WHEN f.member_type = 'AF4M' THEN first_day_favorite ELSE NULL END) AS af4m_first_day_favorite
  ,CAST(af4m_first_day_favorite AS DECIMAL(10,1)) / CAST(af4m_joins AS DECIMAL(10,1)) * 100 AS af4m_favorite_perc
  ,SUM(CASE WHEN f.member_type = 'GM4F' THEN first_day_favorite ELSE NULL END) AS gm4f_first_day_favorite
  ,CAST(gm4f_first_day_favorite AS DECIMAL(10,1)) / CAST(gm4f_joins AS DECIMAL(10,1)) * 100 AS gm4f_favorite_perc
  ,SUM(CASE WHEN f.member_type = 'Other' THEN first_day_favorite ELSE NULL END) AS other_first_day_favorite
  ,CAST(other_first_day_favorite AS DECIMAL(10,1)) / CAST(other_joins AS DECIMAL(10,1)) * 100 AS other_favorite_perc
  ,SUM(membership_purchases) AS first_day_membership_purchases
FROM rollup.sa_landing_and_join_page_sessions AS s
LEFT JOIN seeking_arrange.profiles AS p ON p.uid = s.userid

LEFT JOIN (
SELECT
profile_id
,member_type
,CASE
WHEN account_type = 'generous' THEN (
COALESCE(is_account_type_first_day_completed,0)+
COALESCE(is_birthday_day_first_day_completed,0)+
COALESCE(is_birthday_month_first_day_completed,0)+
COALESCE(is_birthday_year_first_day_completed,0)+
COALESCE(is_body_type_first_day_completed,0)+
COALESCE(is_children_first_day_completed,0)+
COALESCE(is_drinking_first_day_completed,0)+
COALESCE(is_education_first_day_completed,0)+
COALESCE(is_ethnicity_first_day_completed,0)+
COALESCE(is_eye_color_first_day_completed,0)+
COALESCE(is_gender_preference_first_day_completed,0)+
COALESCE(is_hair_color_first_day_completed,0)+
COALESCE(is_height_first_day_completed,0)+
COALESCE(is_income_first_day_completed,0)+
COALESCE(is_industry_first_day_completed,0)+
COALESCE(is_language_first_day_completed,0)+
COALESCE(is_looking_for_tags_first_day_completed,0)+
COALESCE(is_net_worth_first_day_completed,0)+
COALESCE(is_pref_max_age_first_day_completed,0)+
COALESCE(is_pref_min_age_first_day_completed,0)+
COALESCE(is_relationship_status_first_day_completed,0)+
COALESCE(is_seeking_tags_first_day_completed,0)+
COALESCE(is_sex_first_day_completed,0)+
COALESCE(is_smoking_first_day_completed,0)+
COALESCE(is_first_photo_first_day_completed,0)+
COALESCE(is_username_first_day_completed,0)+
COALESCE(is_heading_first_day_completed,0)+
COALESCE(is_looking_for_text_first_day_completed,0)+
COALESCE(is_about_me_first_day_completed,0)+
COALESCE(is_occupation_first_day_completed,0)
)
WHEN account_type = 'attractive' THEN (
COALESCE(is_account_type_first_day_completed,0)+
COALESCE(is_birthday_day_first_day_completed,0)+
COALESCE(is_birthday_month_first_day_completed,0)+
COALESCE(is_birthday_year_first_day_completed,0)+
COALESCE(is_body_type_first_day_completed,0)+
COALESCE(is_children_first_day_completed,0)+
COALESCE(is_drinking_first_day_completed,0)+
COALESCE(is_education_first_day_completed,0)+
COALESCE(is_ethnicity_first_day_completed,0)+
COALESCE(is_eye_color_first_day_completed,0)+
COALESCE(is_gender_preference_first_day_completed,0)+
COALESCE(is_hair_color_first_day_completed,0)+
COALESCE(is_height_first_day_completed,0)+
COALESCE(is_industry_first_day_completed,0)+
COALESCE(is_language_first_day_completed,0)+
COALESCE(is_looking_for_tags_first_day_completed,0)+
COALESCE(is_pref_max_age_first_day_completed,0)+
COALESCE(is_pref_min_age_first_day_completed,0)+
COALESCE(is_relationship_status_first_day_completed,0)+
COALESCE(is_seeking_tags_first_day_completed,0)+
COALESCE(is_sex_first_day_completed,0)+
COALESCE(is_smoking_first_day_completed,0)+
COALESCE(is_first_photo_first_day_completed,0)+
COALESCE(is_username_first_day_completed,0)+
COALESCE(is_heading_first_day_completed,0)+
COALESCE(is_looking_for_text_first_day_completed,0)+
COALESCE(is_about_me_first_day_completed,0)
)
END AS total_first_day_attributes_completed
,CASE
WHEN account_type = 'generous' THEN (COALESCE(total_first_day_attributes_completed,0) / (30::float))
WHEN account_type = 'attractive' THEN (COALESCE(total_first_day_attributes_completed,0) / (27::float))
END AS total_first_day_percent_complete
FROM (
SELECT
spf.profile_id,
p.account_type,
CASE
WHEN p.account_type = 'generous' AND p.sex = 'Male' AND p.profile_attributes ILIKE '%"249"%' THEN 'GM4F'
WHEN p.account_type = 'attractive' AND p.sex = 'Female' AND p.profile_attributes ILIKE '%"248"%' THEN 'AF4M'
ELSE (CASE WHEN p.id IS NOT NULL THEN 'Other' END)
END AS member_type,
CASE WHEN spf.account_type_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.account_type_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_account_type_first_day_completed,
CASE WHEN spf.birthday_day_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.birthday_day_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_birthday_day_first_day_completed,
CASE WHEN spf.birthday_month_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.birthday_month_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_birthday_month_first_day_completed,
CASE WHEN spf.birthday_year_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.birthday_year_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_birthday_year_first_day_completed,
CASE WHEN spf.body_type_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.body_type_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_body_type_first_day_completed,
CASE WHEN spf.children_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.children_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_children_first_day_completed,
CASE WHEN spf.drinking_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.drinking_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_drinking_first_day_completed,
CASE WHEN spf.education_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.education_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_education_first_day_completed,
CASE WHEN spf.ethnicity_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.ethnicity_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_ethnicity_first_day_completed,
CASE WHEN spf.eye_color_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.eye_color_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_eye_color_first_day_completed,
CASE WHEN spf.gender_preference_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.gender_preference_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_gender_preference_first_day_completed,
CASE WHEN spf.hair_color_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.hair_color_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_hair_color_first_day_completed,
CASE WHEN spf.height_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.height_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_height_first_day_completed,
CASE WHEN spf.income_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.income_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_income_first_day_completed,
CASE WHEN spf.indsutry_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.indsutry_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_industry_first_day_completed,
CASE WHEN spf.language_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.language_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_language_first_day_completed,
CASE WHEN spf.looking_for_tags_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.looking_for_tags_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_looking_for_tags_first_day_completed,
CASE WHEN spf.net_worth_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.net_worth_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_net_worth_first_day_completed,
CASE WHEN spf.pref_max_age_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.pref_max_age_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_pref_max_age_first_day_completed,
CASE WHEN spf.pref_min_age_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.pref_min_age_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_pref_min_age_first_day_completed,
CASE WHEN spf.relationship_status_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.relationship_status_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_relationship_status_first_day_completed,
CASE WHEN spf.seeking_tags_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.seeking_tags_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_seeking_tags_first_day_completed,
CASE WHEN spf.sex_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.sex_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_sex_first_day_completed,
CASE WHEN spf.smoking_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.smoking_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_smoking_first_day_completed,
CASE WHEN spf.first_photo_created_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.first_photo_created_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_first_photo_first_day_completed,
CASE WHEN spf.username_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.username_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_username_first_day_completed,
CASE WHEN spf.heading_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.heading_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_heading_first_day_completed,
CASE WHEN spf.looking_for_text_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.looking_for_text_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_looking_for_text_first_day_completed,
CASE WHEN spf.about_me_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.about_me_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_about_me_first_day_completed,
CASE WHEN spf.occupation_first_completed_at IS NOT NULL AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',p.created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.occupation_first_completed_at::timestamp)) = 0 THEN 1 ELSE 0 END AS is_occupation_first_day_completed
FROM rollup.sa_profile_facts AS spf
INNER JOIN seeking_arrange.profiles AS p ON p.id = spf.profile_id
WHERE 1=1
AND spf.profile_created_at >= '2022-01-01T00:00:00.000+00:00'
)
) AS fdpcp ON fdpcp.profile_id = p.id

LEFT JOIN (
SELECT
ap.profile_id,
CASE
WHEN p.account_type = 'generous' AND p.sex = 'Male' AND p.profile_attributes ILIKE '%"249"%' THEN 'GM4F'
WHEN p.account_type = 'attractive' AND p.sex = 'Female' AND p.profile_attributes ILIKE '%"248"%' THEN 'AF4M'
ELSE (CASE WHEN p.id IS NOT NULL THEN 'Other' END)
END AS member_type,
CAST(COUNT(ap.profile_id) AS DECIMAL(10,1)) AS photo_count
FROM rollup.sa_profile_facts AS spf
INNER JOIN seeking_arrange.profiles AS p ON p.id = spf.profile_id
LEFT JOIN seeking_arrange.approvalphotos AS ap ON ap.profile_id = spf.profile_id
WHERE 1=1
AND spf.profile_created_at >= '2022-01-01T00:00:00.000+00:00'
AND DATE_TRUNC('day',ap.created_at) = DATE_TRUNC('day',spf.profile_created_at)
GROUP BY 1,2
ORDER BY 1 DESC
) AS ph ON ph.profile_id = p.id

LEFT JOIN (
SELECT
pv.profile_id,
CASE
WHEN p.account_type = 'generous' AND p.sex = 'Male' AND p.profile_attributes ILIKE '%"249"%' THEN 'GM4F'
WHEN p.account_type = 'attractive' AND p.sex = 'Female' AND p.profile_attributes ILIKE '%"248"%' THEN 'AF4M'
ELSE (CASE WHEN p.id IS NOT NULL THEN 'Other' END)
END AS member_type,
CAST(COUNT(pv.profile_id) AS DECIMAL(10,1)) AS profile_view_count
FROM rollup.sa_profile_facts AS spf
INNER JOIN seeking_arrange.profiles AS p ON p.id = spf.profile_id
INNER JOIN seeking_arrange.profileviews AS pv ON pv.profile_id = spf.profile_id
WHERE 1=1
AND spf.profile_created_at >= '2022-01-01T00:00:00.000+00:00'
AND DATE_TRUNC('day',pv.created_at) = DATE_TRUNC('day',spf.profile_created_at)
GROUP BY 1,2
) AS pv ON pv.profile_id = p.id

LEFT JOIN (
SELECT
py.billable_id AS profile_id,
CASE
WHEN p.account_type = 'generous' AND p.sex = 'Male' AND p.profile_attributes ILIKE '%"249"%' THEN 'GM4F'
WHEN p.account_type = 'attractive' AND p.sex = 'Female' AND p.profile_attributes ILIKE '%"248"%' THEN 'AF4M'
ELSE (CASE WHEN p.id IS NOT NULL THEN 'Other' END)
END AS member_type,
SUM(
CASE
WHEN py.status IN ('APPROVED','SUCCESS','CHARGEBACK-REVERSED')
AND py.amount_usd > 0
AND py.amount <> 0
AND py.type = 'membership'
THEN 1 ELSE 0
END
) AS memb_purchases
FROM rollup.sa_profile_facts AS spf
INNER JOIN seeking_arrange.profiles AS p ON p.id = spf.profile_id
INNER JOIN seeking_arrange.payments AS py ON py.billable_id = spf.profile_id
WHERE 1=1
AND spf.profile_created_at >= '2022-01-01T00:00:00.000+00:00'
AND DATE_TRUNC('day',py.created_at) = DATE_TRUNC('day',spf.profile_created_at)
GROUP BY 1,2
) AS py ON py.profile_id = p.id

LEFT JOIN (
SELECT
spf.profile_id,
CASE
WHEN p.account_type = 'generous' AND p.sex = 'Male' AND p.profile_attributes ILIKE '%"249"%' THEN 'GM4F'
WHEN p.account_type = 'attractive' AND p.sex = 'Female' AND p.profile_attributes ILIKE '%"248"%' THEN 'AF4M'
ELSE (CASE WHEN p.id IS NOT NULL THEN 'Other' END)
END AS member_type,
SUM(pl.amount_usd) AS day_0_revenue
FROM rollup.sa_profile_facts AS spf
INNER JOIN seeking_arrange.profiles AS p ON p.id = spf.profile_id
INNER JOIN seeking_arrange.payments AS py ON py.billable_id = spf.profile_id
INNER JOIN seeking_arrange.paymentlogs AS pl ON pl.payment_id = py.id
WHERE 1=1
AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.profile_created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',pl.created_at::timestamp)) = 0
GROUP BY 1,2
) AS r ON r.profile_id = p.id
s
LEFT JOIN (
SELECT
  spf.profile_id
  ,CASE
    WHEN p.account_type = 'generous' AND p.sex = 'Male' AND p.profile_attributes ILIKE '%"249"%' THEN 'GM4F'
    WHEN p.account_type = 'attractive' AND p.sex = 'Female' AND p.profile_attributes ILIKE '%"248"%' THEN 'AF4M'
    ELSE (CASE WHEN p.id IS NOT NULL THEN 'Other' END)
  END AS member_type
  ,CASE
    WHEN DATE_TRUNC('day',spf.profile_created_at) = DATE_TRUNC('day',spf.first_non_system_message_created_at) THEN 1 ELSE 0
  END AS first_day_message
FROM rollup.sa_profile_facts AS spf
INNER JOIN seeking_arrange.profiles AS p ON p.id = spf.profile_id
WHERE 1=1
  AND spf.profile_created_at >= '2022-01-01T00:00:00.000+00:00'
ORDER BY 1 DESC
) AS m ON m.profile_id = p.id

LEFT JOIN (
SELECT
  spf.profile_id
  ,CASE
    WHEN p.account_type = 'generous' AND p.sex = 'Male' AND p.profile_attributes ILIKE '%"249"%' THEN 'GM4F'
    WHEN p.account_type = 'attractive' AND p.sex = 'Female' AND p.profile_attributes ILIKE '%"248"%' THEN 'AF4M'
    ELSE (CASE WHEN p.id IS NOT NULL THEN 'Other' END)
  END AS member_type
  ,CASE
    WHEN DATE_TRUNC('day',spf.profile_created_at) = DATE_TRUNC('day',spf.first_favorite_sent_at) THEN 1 ELSE 0
  END AS first_day_favorite

FROM rollup.sa_profile_facts AS spf
INNER JOIN seeking_arrange.profiles AS p ON p.id = spf.profile_id
WHERE 1=1
  AND spf.profile_created_at >= '2022-01-01T00:00:00.000+00:00'
ORDER BY 1 DESC
) AS f ON f.profile_id = p.id

LEFT JOIN (
SELECT 
  spf.profile_id
  ,CASE
    WHEN p.account_type = 'generous' AND p.sex = 'Male' AND p.profile_attributes ILIKE '%"249"%' THEN 'GM4F'
    WHEN p.account_type = 'attractive' AND p.sex = 'Female' AND p.profile_attributes ILIKE '%"248"%' THEN 'AF4M'
    ELSE (CASE WHEN p.id IS NOT NULL THEN 'Other' END)
  END AS member_type
  ,COUNT(py.type) AS membership_purchases
FROM rollup.sa_profile_facts AS spf
INNER JOIN seeking_arrange.profiles AS p ON p.id = spf.profile_id
INNER JOIN seeking_arrange.payments AS py ON py.billable_id = p.id AND py.type = 'membership' AND (py.status ilike '%approved%' OR py.status ilike '%success%')
WHERE 1=1
  AND spf.profile_created_at >= '2022-01-01T00:00:00.000+00:00'
  AND DATEDIFF('day', CONVERT_TIMEZONE('UTC','America/Los_Angeles',spf.profile_created_at::timestamp), CONVERT_TIMEZONE('UTC','America/Los_Angeles',py.created_at::timestamp)) = 0
GROUP BY 1,2
) AS pr ON pr.profile_id = p.id

WHERE 1=1
AND first_server_date >= '2022-01-01T00:00:00.000+00:00'
GROUP BY 1
ORDER BY 1 DESC