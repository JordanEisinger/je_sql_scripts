/* ===================================================================================
As of: 07/14/2023

General Notes:
-Modified 'sa_profile_facts' query to include profile attribute and field completed timestamps
-Will be used as base for funnel metrics in subsequent tables

Changelog:
03/15/23: Split out funnel metrics so only timestamps
03/28/23: Added first message, favorite, profile views, saved search, vibe
03/30/23: Updated query structure to use sub-queries as opposed to nested CTE's
04/06/23: Updated select sub-queries to account for data retention
07/14/23: Used Daniel's optimized query posted in DST-2729 ticket comment; updated
    IDV logic to include only 'approved'
=================================================================================== */

WITH id_filter AS (
  SELECT
    profile_id,
    MAX(updated_at) as updated_at 
  FROM(
    SELECT
      p.id AS profile_id,
      p.updated_at  
    FROM seeking_arrange.profiles AS p
    WHERE 1=1
      AND updated_at BETWEEN '{b}' AND '{e}'
    UNION
    SELECT
      pa.billable_id AS profile_id,
      pa.updated_at AS updated_at
    FROM seeking_arrange.payments AS pa
    WHERE 1=1
      AND updated_at BETWEEN '{b}' AND '{e}'
    UNION
    SELECT
      ap.profile_id,
      ap.updated_at AS updated_at
    FROM seeking_arrange.approvalphotos AS ap
    WHERE 1=1
      AND updated_at BETWEEN '{b}' AND '{e}'
    UNION
    SELECT 
      m.profile_id,
      m.updated_at AS updated_at
    FROM seeking_arrange.messages AS m
    WHERE 1=1
      AND updated_at BETWEEN '{b}' AND '{e}'
    UNION
    SELECT
      f.profile_id,
      f.updated_at AS updated_at
    FROM seeking_arrange.favorites AS f
    WHERE 1=1
      AND updated_at BETWEEN '{b}' AND '{e}'
    UNION
    SELECT
      f.member_id AS profile_id,
      f.updated_at AS updated_at
    FROM seeking_arrange.favorites AS f
    WHERE 1=1
      AND updated_at BETWEEN '{b}' AND '{e}'
    UNION
    SELECT
      s.profile_id,
      s.updated_at AS updated_at
    FROM seeking_arrange.savedsearches AS s
    WHERE 1=1
      AND updated_at BETWEEN '{b}' AND '{e}'
    UNION
    SELECT
      v.profile_id,
      v.updated_at AS updated_at
    FROM seeking_arrange.vibes AS v
    WHERE 1=1
      AND updated_at BETWEEN '{b}' AND '{e}'
  )
  GROUP BY 1
)

SELECT
  p.user_id as id,
  p.id as profile_id,
  p.created_at as profile_created_at,
  idf.updated_at as updated_at,
  ub.birthday,  
  ue.email_domain,
  em.user_email_hist,
  json_extract_path_text(p.profile_attributes, 'username', 0, 'value', true) as username,
  json_extract_path_text(p.profile_attributes, 'primary_location', 'country', true) as country,
  pacf.account_type_first_completed_at, 
  pacf.birthday_day_first_completed_at, 
  pacf.birthday_month_first_completed_at, 
  pacf.birthday_year_first_completed_at, 
  pacf.body_type_first_completed_at,
  pacf.children_first_completed_at, 
  pacf.drinking_first_completed_at, 
  pacf.education_first_completed_at, 
  pacf.ethnicity_first_completed_at, 
  pacf.eye_color_first_completed_at,
  pacf.gender_preference_first_completed_at, 
  pacf.hair_color_first_completed_at, 
  pacf.height_first_completed_at, 
  pacf.income_first_completed_at, 
  pacf.indsutry_first_completed_at,
  pacf.language_first_completed_at, 
  pacf.looking_for_tags_first_completed_at, 
  pacf.net_worth_first_completed_at, 
  pacf.pref_max_age_first_completed_at, 
  pacf.pref_min_age_first_completed_at,
  pacf.relationship_status_first_completed_at, 
  pacf.seeking_tags_first_completed_at, 
  pacf.sex_first_completed_at, 
  pacf.smoking_first_completed_at,
  fpu.first_photo_created_at, 
  fpu.first_approved_photo_created_at,
  pfcf.username_first_completed_at, 
  pfcf.heading_first_completed_at, 
  pfcf.looking_for_text_first_completed_at, 
  pfcf.about_me_first_completed_at, 
  pfcf.occupation_first_completed_at,
  fmp.min_membership_payment_created_at,
  ea.email_activation_completed_at,
  fm.first_non_system_message_created_at,
  ffs.first_favorite_sent_at, 
  ffr.first_favorite_received_at,
  pvs.first_profile_view_sent_at, 
  pvr.first_profile_view_received_at,
  ss.first_saved_search_at,
  fv.first_vibe_created_at,
  fid.first_id_verification_created_at
FROM seeking_arrange.profiles p 
INNER JOIN id_filter AS idf ON p.id = idf.profile_id 
-- INNER JOIN seeking_arrange.users u on u.id = p.user_id
LEFT JOIN (
  SELECT
    p.id AS profile_id,
    CASE
      WHEN length(json_extract_path_text(p.profile_attributes, 'birthday_year', 0, 'value', true)) = 4
        AND length(json_extract_path_text(p.profile_attributes, 'birthday_month', 0, 'value', true)) >= 3
        AND length(json_extract_path_text(p.profile_attributes, 'birthday_day', 0, 'value', true)) >= 1
        THEN to_date((json_extract_path_text(p.profile_attributes, 'birthday_year', 0, 'value', true) || '-' || json_extract_path_text(p.profile_attributes, 'birthday_month', 0, 'value', true) || '-' || lpad(json_extract_path_text(p.profile_attributes, 'birthday_day', 0, 'value', true), 2, 0)), 'YYYY-month-DD')
      ELSE NULL
    END AS birthday
  FROM seeking_arrange.profiles p 
  WHERE 1=1
    AND p.id IN (SELECT profile_id FROM id_filter)
) AS ub on ub.profile_id = p.id
LEFT JOIN (
  SELECT
    p.id AS profile_id,
    regexp_replace(split_part(udf.decr(u.email, 'SA'), '@', 2), '\-[A-Za-z0-9]{{6,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{11,}}$', '') as email_domain
  FROM seeking_arrange.profiles p 
  INNER JOIN seeking_arrange.users u on p.user_id = u.id
  WHERE 1=1
    AND p.id IN (SELECT profile_id FROM id_filter)
    AND length(u.email) > 0
) AS ue on ue.profile_id = p.id
LEFT JOIN (
  SELECT
    eh.profile_id,
    split_to_array(listagg(DISTINCT eh.domainname, ', ') within GROUP (ORDER BY eh.profile_id DESC)) AS user_email_hist
  FROM (
    SELECT
      ph.profile_id,
      CASE
        WHEN regexp_replace(split_part(udf.decr(ph.value_crypt, 'SA'), '@', 2), '\-[A-Za-z0-9]{{6,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{11,}}$', '') = '' then null
        ELSE regexp_replace(split_part(udf.decr(ph.value_crypt, 'SA'), '@', 2), '\-[A-Za-z0-9]{{6,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{11,}}$','')
      END as domainname
    FROM seeking_arrange.profilehistory AS ph 
    WHERE 1=1
      AND ph.history_type_id in (7, 19)
      AND ph.profile_id IN (SELECT profile_id FROM id_filter)
  ) eh
  GROUP BY eh.profile_id
) AS em on em.profile_id = p.id
LEFT JOIN (
  SELECT
    ppav.profile_id,
    MIN(CASE WHEN pa.name = 'Account Type' THEN ppav.created_at END) AS account_type_first_completed_at,
    MIN(CASE WHEN pa.name = 'Birthday Day' THEN ppav.created_at END) AS birthday_day_first_completed_at,
    MIN(CASE WHEN pa.name = 'Birthday Month' THEN ppav.created_at END) AS birthday_month_first_completed_at,
    MIN(CASE WHEN pa.name = 'Birthday Year' THEN ppav.created_at END) AS birthday_year_first_completed_at,
    MIN(CASE WHEN pa.name = 'Body Type' THEN ppav.created_at END) AS body_type_first_completed_at,
    MIN(CASE WHEN pa.name = 'Children' THEN ppav.created_at END) AS children_first_completed_at,
    MIN(CASE WHEN pa.name = 'Drinking' THEN ppav.created_at END) AS drinking_first_completed_at,
    MIN(CASE WHEN pa.name = 'Education' THEN ppav.created_at END) AS education_first_completed_at,
    MIN(CASE WHEN pa.name = 'Ethnicity' THEN ppav.created_at END) AS ethnicity_first_completed_at,
    MIN(CASE WHEN pa.name = 'Eye Color' THEN ppav.created_at END) AS eye_color_first_completed_at,
    MIN(CASE WHEN pa.name = 'Gender Preference' THEN ppav.created_at END) AS gender_preference_first_completed_at,
    MIN(CASE WHEN pa.name = 'Hair Color' THEN ppav.created_at END) AS hair_color_first_completed_at,
    MIN(CASE WHEN pa.name = 'Height' THEN ppav.created_at END) AS height_first_completed_at,
    MIN(CASE WHEN pa.name = 'Income' THEN ppav.created_at END) AS income_first_completed_at,
    MIN(CASE WHEN pa.name = 'Industry' THEN ppav.created_at END) AS indsutry_first_completed_at,
    MIN(CASE WHEN pa.name = 'Language' THEN ppav.created_at END) AS language_first_completed_at,
    MIN(CASE WHEN pa.name = 'Looking For Tags' THEN ppav.created_at END) AS looking_for_tags_first_completed_at,
    MIN(CASE WHEN pa.name = 'Net Worth' THEN ppav.created_at END) AS net_worth_first_completed_at,
    MIN(CASE WHEN pa.name = 'Preferred Maximum Age' THEN ppav.created_at END) AS pref_max_age_first_completed_at,
    MIN(CASE WHEN pa.name = 'Preferred Minimum Age' THEN ppav.created_at END) AS pref_min_age_first_completed_at,
    MIN(CASE WHEN pa.name = 'Relationship Status' THEN ppav.created_at END) AS relationship_status_first_completed_at,
    MIN(CASE WHEN pa.name = 'Seeking Tags' THEN ppav.created_at END) AS seeking_tags_first_completed_at,
    MIN(CASE WHEN pa.name = 'Sex' THEN ppav.created_at END) AS sex_first_completed_at,
    MIN(CASE WHEN pa.name = 'Smoking' THEN ppav.created_at END) AS smoking_first_completed_at
  FROM seeking_arrange.profile_profileattributevalue AS ppav
  INNER JOIN seeking_arrange.profileattributevalues AS pav ON pav.id = ppav.profileattributevalue_id
  INNER JOIN seeking_arrange.profileattributes AS pa ON pa.id = pav.profileattribute_id
  WHERE 1=1
    AND ppav.profile_id IN (SELECT profile_id FROM id_filter)
  GROUP BY 1
) AS pacf ON pacf.profile_id = p.id
LEFT JOIN (
  SELECT 
    ap.profile_id,
    MIN(ap.created_at) AS first_photo_created_at,
    MIN(CASE WHEN ap.moderated_status = 1 THEN ap.created_at END) AS first_approved_photo_created_at
  FROM seeking_arrange.approvalphotos AS ap 
  WHERE 1=1
    AND ap.profile_id IN (SELECT profile_id FROM id_filter)
  GROUP BY 1
) AS fpu ON fpu.profile_id = p.id
LEFT JOIN (
  SELECT 
    pfv.profile_id,
    MIN(CASE WHEN name = 'Username' THEN pfv.created_at END) AS username_first_completed_at,
    MIN(CASE WHEN name = 'Heading' THEN pfv.created_at END) AS heading_first_completed_at,
    MIN(CASE WHEN name = 'Looking For' AND value <> ' ' THEN pfv.created_at END) AS looking_for_text_first_completed_at,
    MIN(CASE WHEN name = 'About Me' AND value <> ' ' THEN pfv.created_at END) AS about_me_first_completed_at,
    MIN(CASE WHEN name = 'Occupation' THEN pfv.created_at END) AS occupation_first_completed_at
  FROM seeking_arrange.profilefieldvalues AS pfv 
  INNER JOIN seeking_arrange.profileattributes AS pa ON pa.id = pfv.profileattribute_id
  WHERE 1=1
    AND pfv.profile_id IN (SELECT profile_id FROM id_filter)
  GROUP BY 1
) AS pfcf ON pfcf.profile_id = p.id
LEFT JOIN (
  SELECT
    p.billable_id AS profile_id,
    MIN(p.created_at) AS min_membership_payment_created_at
  FROM seeking_arrange.payments AS p 
  WHERE 1=1
    AND p.billable_id IN (SELECT profile_id FROM id_filter)
    AND p.type = 'membership' 
    AND p.status IN ('APPROVED','SUCCESS')
  GROUP BY 1
) AS fmp ON fmp.profile_id = p.id
LEFT JOIN (
  SELECT 
    p.id AS profile_id,
    MIN(a.completed_at) AS email_activation_completed_at
  FROM seeking_arrange.profiles AS p
  INNER JOIN seeking_arrange.activations AS a ON a.user_id = p.user_id
  WHERE 1=1
    AND p.id IN (SELECT profile_id FROM id_filter)
  GROUP BY 1
) AS ea ON ea.profile_id = p.id
LEFT JOIN (
  SELECT 
    m.profile_id,
    MIN(m.created_at) AS first_non_system_message_created_at
  FROM seeking_arrange.messages AS m 
  WHERE 1=1
    AND m.is_system = 0
    AND m.profile_id IN (SELECT profile_id FROM rollup.sa_profile_facts WHERE profile_id IN (SELECT profile_id FROM id_filter) AND first_non_system_message_created_at IS NULL)
  GROUP BY 1
) AS fm ON fm.profile_id = p.id
LEFT JOIN (
  SELECT
    f.profile_id,
    MIN(f.created_at) AS first_favorite_sent_at
  FROM seeking_arrange.favorites AS f 
  WHERE 1=1
    AND f.profile_id IN (SELECT profile_id FROM rollup.sa_profile_facts WHERE profile_id IN (SELECT profile_id FROM id_filter) AND first_favorite_sent_at IS NULL)
  GROUP BY 1
) AS ffs ON ffs.profile_id = p.id
LEFT JOIN (
  SELECT
    f.member_id AS profile_id,
    MIN(f.created_at) AS first_favorite_received_at
  FROM seeking_arrange.favorites AS f 
  WHERE 1=1
    AND f.member_id IN (SELECT profile_id FROM rollup.sa_profile_facts WHERE profile_id IN (SELECT profile_id FROM id_filter) AND first_favorite_received_at IS NULL)
  GROUP BY 1
) AS ffr ON ffr.profile_id = p.id
LEFT JOIN (
  SELECT
    pv.profile_id,
    MIN(pv.created_at) AS first_profile_view_sent_at
  FROM seeking_arrange.profileviews AS pv 
  WHERE 1=1
    AND pv.profile_id IN (SELECT profile_id FROM rollup.sa_profile_facts WHERE profile_id IN (SELECT profile_id FROM id_filter) AND first_profile_view_sent_at IS NULL)
  GROUP BY 1
) AS pvs ON pvs.profile_id = p.id
LEFT JOIN (
  SELECT
    pv.member_profile_id AS profile_id,
    MIN(pv.created_at) AS first_profile_view_received_at
  FROM seeking_arrange.profileviews AS pv 
  WHERE 1=1
    AND pv.member_profile_id IN (SELECT profile_id FROM rollup.sa_profile_facts WHERE profile_id IN (SELECT profile_id FROM id_filter) AND first_profile_view_received_at IS NULL)
  GROUP BY 1
) AS pvr ON pvr.profile_id = p.id
LEFT JOIN (
  SELECT 
    ss.profile_id,
    MIN(ss.created_at) AS first_saved_search_at
  FROM seeking_arrange.savedsearches AS ss
  WHERE 1=1
    AND ss.profile_id IN (SELECT profile_id FROM id_filter)
  GROUP BY 1
) AS ss ON ss.profile_id = p.id
LEFT JOIN (
  SELECT
    v.profile_id,
    MIN(v.created_at) AS first_vibe_created_at
  FROM seeking_arrange.vibes AS v 
  WHERE 1=1
    AND v.profile_id IN (SELECT profile_id FROM id_filter)
  GROUP BY 1
) AS fv ON fv.profile_id = p.id
LEFT JOIN (
  SELECT
    vi.profile_id,
    MIN(vi.created_at) AS first_id_verification_created_at
  FROM seeking_arrange.verificationidentifications AS vi
  WHERE 1=1
    AND vi.status = 'approved'
    AND vi.profile_id IN (SELECT profile_id FROM id_filter)
  GROUP BY 1
) AS fid ON fid.profile_id = p.id