/* ===================================================================================
As of: 03/15/2023

General Notes:
-Modified 'sa_profile_facts' query to include profile attribute and field completed timestamps
-Will be used as base for funnel metrics in subsequent tables

Changelog:
03/15/23: Split out funnel metrics so only timestamps
03/28/23: Added first message, favorite, profile views, saved search, vibe
=================================================================================== */

WITH id_filter AS (
  SELECT
    profile_id,
    MAX(updated_at) as updated_at 
  FROM(
    SELECT
      p.id AS profile_id,
      p.updated_at
    FROM seeking_arrange.profiles AS p
    WHERE 1=1
      AND updated_at between '{b}' and '{e}'
    UNION
    SELECT
      pa.billable_id AS profile_id,
      pa.updated_at AS updated_at
    FROM seeking_arrange.payments AS pa
    WHERE 1=1
      AND updated_at between '{b}' and '{e}'
    UNION
    SELECT
      ap.profile_id,
      ap.updated_at AS updated_at
    FROM seeking_arrange.approvalphotos AS ap
    WHERE 1=1
      AND updated_at between '{b}' and '{e}'
  )
  GROUP BY 1
),
user_email AS (
  SELECT
    idf.profile_id,
    regexp_replace(split_part(udf.decr(u.email, 'SA'), '@', 2), '\-[A-Za-z0-9]{{6,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{11,}}$', '') as email_domain
  FROM id_filter idf
  INNER JOIN seeking_arrange.profiles p on idf.profile_id = p.id
  INNER JOIN seeking_arrange.users u on p.user_id = u.id
  WHERE 1=1
    AND length(u.email) > 0
),
email_history AS (
  SELECT
    eh.profile_id,
    split_to_array(listagg(DISTINCT eh.domainname, ', ') within GROUP (ORDER BY eh.profile_id DESC)) AS user_email_hist
  FROM (
    SELECT
      idf.profile_id,
      CASE
        WHEN regexp_replace(split_part(udf.decr(ph.value_crypt, 'SA'), '@', 2), '\-[A-Za-z0-9]{{6,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{11,}}$', '') = '' then null
        ELSE regexp_replace(split_part(udf.decr(ph.value_crypt, 'SA'), '@', 2), '\-[A-Za-z0-9]{{6,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{4,}}\-[A-Za-z0-9]{{11,}}$','')
      END as domainname
    FROM id_filter idf
    INNER JOIN seeking_arrange.profilehistory ph on idf.profile_id = ph.profile_id
    WHERE 1=1
      AND ph.history_type_id in (7, 19)
  ) eh
  GROUP BY eh.profile_id
),
user_birthday AS (
  SELECT
    p.id AS profile_id,
    CASE
      WHEN length(json_extract_path_text(p.profile_attributes, 'birthday_year', 0, 'value', true)) = 4
        AND length(json_extract_path_text(p.profile_attributes, 'birthday_month', 0, 'value', true)) >= 3
        AND length(json_extract_path_text(p.profile_attributes, 'birthday_day', 0, 'value', true)) >= 1
        THEN to_date((json_extract_path_text(p.profile_attributes, 'birthday_year', 0, 'value', true) || '-' || json_extract_path_text(p.profile_attributes, 'birthday_month', 0, 'value', true) || '-' || lpad(json_extract_path_text(p.profile_attributes, 'birthday_day', 0, 'value', true), 2, 0)), 'YYYY-month-DD')
      ELSE NULL
    END AS birthday
  FROM id_filter idf
  INNER JOIN seeking_arrange.profiles p ON idf.profile_id = p.id
), 
first_photo_uploads AS (
  SELECT 
    ap.profile_id,
    MIN(ap.created_at) AS first_photo_created_at,
    MIN(CASE WHEN ap.moderated_status = 1 THEN ap.created_at END) AS first_approved_photo_created_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.approvalphotos AS ap ON ap.profile_id = idf.profile_id
  GROUP BY 1
),
profile_attribute_completion_flags AS (
  SELECT
    ppav.profile_id,
    MIN(CASE WHEN pa.name = 'Account Type' THEN ppav.created_at ELSE NULL END) AS account_type_first_completed_at,
    MIN(CASE WHEN pa.name = 'Birthday Day' THEN ppav.created_at ELSE NULL END) AS birthday_day_first_completed_at,
    MIN(CASE WHEN pa.name = 'Birthday Month' THEN ppav.created_at ELSE NULL END) AS birthday_month_first_completed_at,
    MIN(CASE WHEN pa.name = 'Birthday Year' THEN ppav.created_at ELSE NULL END) AS birthday_year_first_completed_at,
    MIN(CASE WHEN pa.name = 'Body Type' THEN ppav.created_at ELSE NULL END) AS body_type_first_completed_at,
    MIN(CASE WHEN pa.name = 'Children' THEN ppav.created_at ELSE NULL END) AS children_first_completed_at,
    MIN(CASE WHEN pa.name = 'Drinking' THEN ppav.created_at ELSE NULL END) AS drinking_first_completed_at,
    MIN(CASE WHEN pa.name = 'Education' THEN ppav.created_at ELSE NULL END) AS education_first_completed_at,
    MIN(CASE WHEN pa.name = 'Ethnicity' THEN ppav.created_at ELSE NULL END) AS ethnicity_first_completed_at,
    MIN(CASE WHEN pa.name = 'Eye Color' THEN ppav.created_at ELSE NULL END) AS eye_color_first_completed_at,
    MIN(CASE WHEN pa.name = 'Gender Preference' THEN ppav.created_at ELSE NULL END) AS gender_preference_first_completed_at,
    MIN(CASE WHEN pa.name = 'Hair Color' THEN ppav.created_at ELSE NULL END) AS hair_color_first_completed_at,
    MIN(CASE WHEN pa.name = 'Height' THEN ppav.created_at ELSE NULL END) AS height_first_completed_at,
    MIN(CASE WHEN pa.name = 'Income' THEN ppav.created_at ELSE NULL END) AS income_first_completed_at,
    MIN(CASE WHEN pa.name = 'Industry' THEN ppav.created_at ELSE NULL END) AS indsutry_first_completed_at,
    MIN(CASE WHEN pa.name = 'Language' THEN ppav.created_at ELSE NULL END) AS language_first_completed_at,
    MIN(CASE WHEN pa.name = 'Looking For Tags' THEN ppav.created_at ELSE NULL END) AS looking_for_tags_first_completed_at,
    MIN(CASE WHEN pa.name = 'Net Worth' THEN ppav.created_at ELSE NULL END) AS net_worth_first_completed_at,
    MIN(CASE WHEN pa.name = 'Preferred Maximum Age' THEN ppav.created_at ELSE NULL END) AS pref_max_age_first_completed_at,
    MIN(CASE WHEN pa.name = 'Preferred Minimum Age' THEN ppav.created_at ELSE NULL END) AS pref_min_age_first_completed_at,
    MIN(CASE WHEN pa.name = 'Relationship Status' THEN ppav.created_at ELSE NULL END) AS relationship_status_first_completed_at,
    MIN(CASE WHEN pa.name = 'Seeking Tags' THEN ppav.created_at ELSE NULL END) AS seeking_tags_first_completed_at,
    MIN(CASE WHEN pa.name = 'Sex' THEN ppav.created_at ELSE NULL END) AS sex_first_completed_at,
    MIN(CASE WHEN pa.name = 'Smoking' THEN ppav.created_at ELSE NULL END) AS smoking_first_completed_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.profile_profileattributevalue AS ppav ON ppav.profile_id = idf.profile_id
  INNER JOIN seeking_arrange.profileattributevalues AS pav ON pav.id = ppav.profileattributevalue_id
  INNER JOIN seeking_arrange.profileattributes AS pa ON pa.id = pav.profileattribute_id
  GROUP BY 1
),
profile_fields_completion_flags AS (
  SELECT 
    pfv.profile_id,
    MIN(CASE WHEN name = 'Username' THEN pfv.created_at END) AS username_first_completed_at,
    MIN(CASE WHEN name = 'Heading' THEN pfv.created_at END) AS heading_first_completed_at,
    MIN(CASE WHEN name = 'Looking For' AND value <> ' ' THEN pfv.created_at END) AS looking_for_text_first_completed_at,
    MIN(CASE WHEN name = 'About Me' AND value <> ' ' THEN pfv.created_at END) AS about_me_first_completed_at,
    MIN(CASE WHEN name = 'Occupation' THEN pfv.created_at END) AS occupation_first_completed_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.profilefieldvalues AS pfv ON pfv.profile_id = idf.profile_id
  INNER JOIN seeking_arrange.profileattributes AS pa ON pa.id = pfv.profileattribute_id
  GROUP BY 1
), 
first_membership_purchase AS (
  SELECT
    p.billable_id AS profile_id,
    MIN(CASE WHEN p.type = 'membership' AND p.status ilike '%approved%' OR p.status ilike '%success%' THEN p.created_at ELSE NULL END) AS min_membership_payment_created_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.payments AS p ON p.billable_id = idf.profile_id
  GROUP BY 1
), 
email_activations AS (
  SELECT 
    idf.profile_id,
    MIN(a.completed_at) AS email_activation_completed_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.profiles AS p ON p.id = idf.profile_id
  INNER JOIN seeking_arrange.activations AS a ON a.user_id = p.user_id
  GROUP BY 1
),
first_non_sysytem_message AS (
  SELECT 
    idf.profile_id,
    MIN(m.created_at) AS first_non_system_message_created_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.messages AS m ON m.profile_id = idf.profile_id AND m.is_system = 0
  GROUP BY 1
),
first_favorite_sent AS (
  SELECT
    idf.profile_id,
    MIN(f.created_at) AS first_favorite_sent_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.favorites AS f ON f.profile_id = idf.profile_id
  GROUP BY 1
),
first_favorite_received AS (
  SELECT
    idf.profile_id,
    MIN(f.created_at) AS first_favorite_received_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.favorites AS f ON f.member_id = idf.profile_id
  GROUP BY 1
),
first_profile_view_sent AS (
  SELECT
    idf.profile_id,
    MIN(pv.created_at) AS first_profile_view_sent_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.profileviews AS pv ON pv.profile_id = idf.profile_id
  GROUP BY 1
),
first_profile_view_received AS (
  SELECT
    idf.profile_id,
    MIN(pv.created_at) AS first_profile_view_received_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.profileviews AS pv ON pv.member_id = idf.profile_id
  GROUP BY 1
),
first_saved_search AS (
  SELECT 
    idf.profile_id,
    MIN(pv.created_at) AS first_saved_search_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.savedsearches AS ss ON ss.profile_id = idf.profile_id
  GROUP BY 1
),
first_vibe AS (
  SELECT
    idf.profile_id,
    MIN(v.created_at) AS first_vibe_created_at
  FROM id_filter AS idf
  INNER JOIN seeking_arrange.vibes AS v ON v.profile_id = idf.profile_id
  GROUP BY 1
)

SELECT
  u.id as id,
  p.id as profile_id,
  ub.birthday,  
  ue.email_domain,
  em.user_email_hist,
  p.created_at as profile_created_at,
  idf.updated_at as updated_at,
  json_extract_path_text(p.profile_attributes, 'username', 0, 'value', true) as username,
  json_extract_path_text(p.profile_attributes, 'primary_location', 'country', true) as country,
  pacf.account_type_first_completed_at, pacf.birthday_day_first_completed_at, pacf.birthday_month_first_completed_at, pacf.birthday_year_first_completed_at, pacf.body_type_first_completed_at,
  pacf.children_first_completed_at, pacf.drinking_first_completed_at, pacf.education_first_completed_at, pacf.ethnicity_first_completed_at, pacf.eye_color_first_completed_at,
  pacf.gender_preference_first_completed_at, pacf.hair_color_first_completed_at, pacf.height_first_completed_at, pacf.income_first_completed_at, pacf.indsutry_first_completed_at,
  pacf.language_first_completed_at, pacf.looking_for_tags_first_completed_at, pacf.net_worth_first_completed_at, pacf.pref_max_age_first_completed_at, pacf.pref_min_age_first_completed_at,
  pacf.relationship_status_first_completed_at, pacf.seeking_tags_first_completed_at, pacf.sex_first_completed_at, pacf.smoking_first_completed_at,
  fpu.first_photo_created_at, fpu.first_approved_photo_created_at,
  pfcf.username_first_completed_at, pfcf.heading_first_completed_at, pfcf.looking_for_text_first_completed_at, pfcf.about_me_first_completed_at, pfcf.occupation_first_completed_at,
  fmp.min_membership_payment_created_at,
  ea.email_activation_completed_at,
  fm.first_non_system_message_created_at,
  ff.first_favorite_sent_at, ff.first_favorite_received_at,
  pvr.first_profile_view_sent_at, pvr.first_profile_view_received_at,
  ss.first_saved_search_at,
  fv.first_vibe_created_at
FROM id_filter idf
INNER JOIN seeking_arrange.profiles p on p.id = idf.profile_id
INNER JOIN seeking_arrange.users u on u.id = p.user_id
LEFT JOIN user_birthday ub on ub.profile_id = idf.profile_id
LEFT JOIN user_email ue on ue.profile_id = idf.profile_id
LEFT JOIN email_history em on em.profile_id = idf.profile_id
LEFT JOIN profile_attribute_completion_flags AS pacf ON pacf.profile_id = idf.profile_id
LEFT JOIN first_photo_uploads AS fpu ON fpu.profile_id = idf.profile_id
LEFT JOIN profile_fields_completion_flags AS pfcf ON pfcf.profile_id = idf.profile_id
LEFT JOIN first_membership_purchase AS fmp ON fmp.profile_id = idf.profile_id
LEFT JOIN email_activations AS ea ON ea.profile_id = idf.profile_id
LEFT JOIN first_non_sysytem_message AS fm ON fm.profile_id = idf.profile_id
LEFT JOIN first_favorite_sent AS ffs ON ffs.profile_id = idf.profile_id
LEFT JOIN first_favorite_received AS ffr ON ffr.profile_id = idf.profile_id
LEFT JOIN first_profile_view_sent AS pvs ON pvs.profile_id = idf.profile_id
LEFT JOIN first_profile_view_received AS pvr ON pvr.profile_id = idf.profile_id
LEFT JOIN first_saved_search AS ss ON ss.profile_id = idf.profile_id
LEFT JOIN first_vibe AS fv ON fv.profile_id = idf.profile_id