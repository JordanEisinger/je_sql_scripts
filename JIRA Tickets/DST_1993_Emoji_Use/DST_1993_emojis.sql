/* ===================================================================================
As of: 07/26/2022 

General Notes:
-Fraud delete OR suspensions, not AND (two different Looks?)
-Future steps: look for ones that haven’t been found yet (non-fraud/suspended users)
-Group and quantify by counts of offenses (which emoji’s are associated with fraud delete and suspensions reasons, count of profiles)
-Drug use not as big of a concern as selling (focus on emoji’s centered around dollars, phone, car, etc.)
-Focus on groups of emojis (emojis combined with other emojis have a higher chance of offense)

URLs: 
- https://www.cs.cmu.edu/~pattis/15-1XX/common/handouts/ascii.html
- https://www.alt-codes.net/smiley_alt_codes.php

Changelog:
07/26/2022: Added REGEXP logic to SA Messages (RS) Looker View file
=================================================================================== */


-----------------------
----- SUSPENSIONS -----
-----------------------
-- SELECT * FROM seeking_arrange.suspendreasons ORDER BY id DESC LIMIT 50
-- SELECT * FROM seeking_arrange.suspensions LIMIT 50


-----------------------
---- FRAUD DELETES ----
-----------------------
--> Start with only fraud deleted accounts (~29K as of 07/11/22)
WITH fraud_deletes AS (
  SELECT 
    fd.*,
    fdr.name
  FROM seeking_arrange.frauddeletes AS fd
  LEFT JOIN frauddeletereasons AS fdr ON fdr.id = fd.fraud_delete_reason_id 
  WHERE 1=1
    AND fd.created_at >= '2022-07-01T00:00:00.000+00:00'
),
fraud_delete_messages AS (
--> Get messages only from fraud deleted accounts
--> ~1.4M total as of 07/11/22
SELECT 
  m.*,
  fd.name AS fraud_delete_reason,
  fd.created_at AS fraud_deleted_at
FROM seeking_arrange.messages AS m
INNER JOIN fraud_deletes AS fd ON fd.member_profile_id = m.profile_id
WHERE 1=1
  AND m.created_at >= '2022-07-01T00:00:00.000+00:00'
  --> Only returns messages that contain an emoji
  AND REGEXP_INSTR(m.body,'[^'||chr(1)||'-'||chr(127000)||']')
LIMIT 500
)
--> Main select query
SELECT
  fdm.id AS messaged_id,
  fdm.conversation_id,
  fdm.profile_id,
  fdm.fraud_delete_reason,
  fdm.fraud_deleted_at,
  fdm.body AS message_body,
  fdm.created_at AS message_created_at,
  fdm.viewed_at AS message_viewed_at,
  -- > Returns first emoji that was used
  REGEXP_SUBSTR(fdm.body,'[^'||chr(1)||'-'||chr(127000)||']') AS first_emoji_used,
  --> Returns all emojis used
  REGEXP_REPLACE(fdm.body,'[^'||chr(10000)||'-'||chr(200000)||']') AS all_emojis_used,
  --> Returns number of emojis used
  REGEXP_COUNT(fdm.body,'[^'||chr(1)||'-'||chr(127000)||']') AS emoji_count,
  --> Assigns numeric value to each emoji to be summed in subsequent query
  CASE WHEN all_emojis_used ilike '%😈%' THEN 1 ELSE 0 END AS num_emoji_😈,
  CASE WHEN all_emojis_used ilike '%💯%' THEN 1 ELSE 0 END AS num_emoji_💯,
  CASE WHEN all_emojis_used ilike '%🍌%' THEN 1 ELSE 0 END AS num_emoji_🍌,
  CASE WHEN all_emojis_used ilike '%🔵%' THEN 1 ELSE 0 END AS num_emoji_🔵,
  CASE WHEN all_emojis_used ilike '%💣%' THEN 1 ELSE 0 END AS num_emoji_💣,
  CASE WHEN all_emojis_used ilike '%📦%' THEN 1 ELSE 0 END AS num_emoji_📦,
  CASE WHEN all_emojis_used ilike '%🥦%' THEN 1 ELSE 0 END AS num_emoji_🥦,
  CASE WHEN all_emojis_used ilike '%🎯%' THEN 1 ELSE 0 END AS num_emoji_🎯,
  CASE WHEN all_emojis_used ilike '%🍬%' THEN 1 ELSE 0 END AS num_emoji_🍬,
  CASE WHEN all_emojis_used ilike '%🛒%' THEN 1 ELSE 0 END AS num_emoji_🛒,
  CASE WHEN all_emojis_used ilike '%💵%' THEN 1 ELSE 0 END AS num_emoji_💵,
  CASE WHEN all_emojis_used ilike '%🐥%' THEN 1 ELSE 0 END AS num_emoji_🐥,
  CASE WHEN all_emojis_used ilike '%🍫%' THEN 1 ELSE 0 END AS num_emoji_🍫,
  CASE WHEN all_emojis_used ilike '%🎄%' THEN 1 ELSE 0 END AS num_emoji_🎄,
  CASE WHEN all_emojis_used ilike '%🚬%' THEN 1 ELSE 0 END AS num_emoji_🚬,
  CASE WHEN all_emojis_used ilike '%🥥%' THEN 1 ELSE 0 END AS num_emoji_🥥,
  CASE WHEN all_emojis_used ilike '%🚗%' THEN 1 ELSE 0 END AS num_emoji_🚗,
  CASE WHEN all_emojis_used ilike '%📲%' THEN 1 ELSE 0 END AS num_emoji_📲,
  CASE WHEN all_emojis_used ilike '%🪂%' THEN 1 ELSE 0 END AS num_emoji_🪂,
  CASE WHEN all_emojis_used ilike '%🏪%' THEN 1 ELSE 0 END AS num_emoji_🏪,
  CASE WHEN all_emojis_used ilike '%🍪%' THEN 1 ELSE 0 END AS num_emoji_🍪,
  CASE WHEN all_emojis_used ilike '%👑%' THEN 1 ELSE 0 END AS num_emoji_👑,
  CASE WHEN all_emojis_used ilike '%💎%' THEN 1 ELSE 0 END AS num_emoji_💎,
  CASE WHEN all_emojis_used ilike '%🐉%' THEN 1 ELSE 0 END AS num_emoji_🐉,
  CASE WHEN all_emojis_used ilike '%🎱%' THEN 1 ELSE 0 END AS num_emoji_🎱,
  CASE WHEN all_emojis_used ilike '%💥%' THEN 1 ELSE 0 END AS num_emoji_💥,
  CASE WHEN all_emojis_used ilike '%🔥%' THEN 1 ELSE 0 END AS num_emoji_🔥,
  CASE WHEN all_emojis_used ilike '%🍀%' THEN 1 ELSE 0 END AS num_emoji_🍀,
  CASE WHEN all_emojis_used ilike '%⛽%' THEN 1 ELSE 0 END AS num_emoji_⛽,
  CASE WHEN all_emojis_used ilike '%👻%' THEN 1 ELSE 0 END AS num_emoji_👻 ,
  CASE WHEN all_emojis_used ilike '%🍇%' THEN 1 ELSE 0 END AS num_emoji_🍇,
  CASE WHEN all_emojis_used ilike '%🟢%' THEN 1 ELSE 0 END AS num_emoji_🟢,
  CASE WHEN all_emojis_used ilike '%💉%' THEN 1 ELSE 0 END AS num_emoji_💉,
  CASE WHEN all_emojis_used ilike '%🍯,%' THEN 1 ELSE 0 END AS num_emoji_🍯,
  CASE WHEN all_emojis_used ilike '%🐎%' THEN 1 ELSE 0 END AS num_emoji_🐎,
  CASE WHEN all_emojis_used ilike '%🔑%' THEN 1 ELSE 0 END AS num_emoji_🔑,
  CASE WHEN all_emojis_used ilike '%🚆%' THEN 1 ELSE 0 END AS num_emoji_🚆,
  CASE WHEN all_emojis_used ilike '%💡%' THEN 1 ELSE 0 END AS num_emoji_💡,
  CASE WHEN all_emojis_used ilike '%⚡%' THEN 1 ELSE 0 END AS num_emoji_⚡,
  CASE WHEN all_emojis_used ilike '%🍁%' THEN 1 ELSE 0 END AS num_emoji_🍁,
  CASE WHEN all_emojis_used ilike '%💰%' THEN 1 ELSE 0 END AS num_emoji_💰,
  CASE WHEN all_emojis_used ilike '%🏔%' THEN 1 ELSE 0 END AS num_emoji_🏔,
  CASE WHEN all_emojis_used ilike '%🍄%' THEN 1 ELSE 0 END AS num_emoji_🍄,
  CASE WHEN all_emojis_used ilike '%👃%' THEN 1 ELSE 0 END AS num_emoji_👃,
  CASE WHEN all_emojis_used ilike '%🌴%' THEN 1 ELSE 0 END AS num_emoji_🌴,
  CASE WHEN all_emojis_used ilike '%🅿️%' THEN 1 ELSE 0 END AS num_emoji_🅿️,
  CASE WHEN all_emojis_used ilike '%🥧%' THEN 1 ELSE 0 END AS num_emoji_🥧,
  CASE WHEN all_emojis_used ilike '%🐷%' THEN 1 ELSE 0 END AS num_emoji_🐷,
  CASE WHEN all_emojis_used ilike '%💊%' THEN 1 ELSE 0 END AS num_emoji_💊,
  CASE WHEN all_emojis_used ilike '%🔌,%' THEN 1 ELSE 0 END AS num_emoji_🔌,
  CASE WHEN all_emojis_used ilike '%📮%' THEN 1 ELSE 0 END AS num_emoji_📮,
  CASE WHEN all_emojis_used ilike '%🙏%' THEN 1 ELSE 0 END AS num_emoji_🙏,
  CASE WHEN all_emojis_used ilike '%🐀%' THEN 1 ELSE 0 END AS num_emoji_🐀,
  CASE WHEN all_emojis_used ilike '%🍚%' THEN 1 ELSE 0 END AS num_emoji_🍚,
  CASE WHEN all_emojis_used ilike '%🚀%' THEN 1 ELSE 0 END AS num_emoji_🚀,
  CASE WHEN all_emojis_used ilike '%🚌%' THEN 1 ELSE 0 END AS num_emoji_🚌,
  CASE WHEN all_emojis_used ilike '%💨%' THEN 1 ELSE 0 END AS num_emoji_💨,
  CASE WHEN all_emojis_used ilike '%🐍%' THEN 1 ELSE 0 END AS num_emoji_🐍,
  CASE WHEN all_emojis_used ilike '%🤧%' THEN 1 ELSE 0 END AS num_emoji_🤧,
  CASE WHEN all_emojis_used ilike '%❄️%' THEN 1 ELSE 0 END AS num_emoji_❄️,
  CASE WHEN all_emojis_used ilike '%🌨️%' THEN 1 ELSE 0 END AS num_emoji_🌨️,
  CASE WHEN all_emojis_used ilike '%⛄%' THEN 1 ELSE 0 END AS num_emoji_⛄,
  CASE WHEN all_emojis_used ilike '%🌲%' THEN 1 ELSE 0 END AS num_emoji_🌲,
  CASE WHEN all_emojis_used ilike '%⚪%' THEN 1 ELSE 0 END AS num_emoji_⚪,
  CASE WHEN all_emojis_used ilike '%🌿%' THEN 1 ELSE 0 END AS num_emoji_🌿,
  CASE WHEN all_emojis_used ilike '%🍃%' THEN 1 ELSE 0 END AS num_emoji_🍃,
  CASE WHEN all_emojis_used ilike '%⚗️%' THEN 1 ELSE 0 END AS num_emoji_⚗️,
  CASE WHEN all_emojis_used ilike '%🤑%' THEN 1 ELSE 0 END AS num_emoji_🤑,
  CASE WHEN all_emojis_used ilike '%🤯%' THEN 1 ELSE 0 END AS num_emoji_🤯,
  CASE WHEN all_emojis_used ilike '%🌳%' THEN 1 ELSE 0 END AS num_emoji_🌳,
  CASE WHEN all_emojis_used ilike '%🌱%' THEN 1 ELSE 0 END AS num_emoji_🌱
FROM fraud_delete_messages AS fdm
ORDER BY 1 DESC


------------------------------
---- FRAUD DELETE REASONS ----
------------------------------
--> Messages with emojis
WITH emoji_messages AS (
  --> ~15M+ messages in total as of 07/11/22
  SELECT *
  FROM seeking_arrange.messages AS m
  WHERE 1=1
    --> Only returns messages that contain an emoji (need to verify upper limit value gets all emojis)
    AND REGEXP_INSTR(m.body,'[^'||chr(1)||'-'||chr(127000)||']')
    --> Restricts to certain date range
    AND created_at >= '2022-01-01T00:00:00.000+00:00'
  LIMIT 100000
),
--> Fraud deletes users
fraud_deletes AS (
  SELECT * 
  FROM seeking_arrange.frauddeletes AS fd
  LEFT JOIN frauddeletereasons AS fdr ON fdr.id = fd.fraud_delete_reason_id 
  WHERE 1=1
    AND fd.created_at >= '2022-01-01T00:00:00.000+00:00'
),
emojis_used AS (
  SELECT 
    em.*,
    fd.name AS fraud_delete_reason,
    --> Returns first emoji that was used
    REGEXP_SUBSTR(em.body,'[^'||chr(1)||'-'||chr(127000)||']') AS first_emoji_used,
    --> Returns number of emojis used
    REGEXP_COUNT(em.body,'[^'||chr(1)||'-'||chr(127000)||']') AS emoji_count,
    --> Returns all emojis used (need to verify ASCII range)
    REGEXP_REPLACE(em.body,'[^'||chr(10000)||'-'||chr(200000)||']') AS all_emojis_used,
    --> Assigns numeric value to each emoji to be summed in subsequent query
    CASE WHEN all_emojis_used ilike '%😈%' THEN 1 ELSE 0 END AS num_emoji_😈,
    CASE WHEN all_emojis_used ilike '%💯%' THEN 1 ELSE 0 END AS num_emoji_💯,
    CASE WHEN all_emojis_used ilike '%🍌%' THEN 1 ELSE 0 END AS num_emoji_🍌,
    CASE WHEN all_emojis_used ilike '%🔵%' THEN 1 ELSE 0 END AS num_emoji_🔵,
    CASE WHEN all_emojis_used ilike '%💣%' THEN 1 ELSE 0 END AS num_emoji_💣,
    CASE WHEN all_emojis_used ilike '%📦%' THEN 1 ELSE 0 END AS num_emoji_📦,
    CASE WHEN all_emojis_used ilike '%🥦%' THEN 1 ELSE 0 END AS num_emoji_🥦,
    CASE WHEN all_emojis_used ilike '%🎯%' THEN 1 ELSE 0 END AS num_emoji_🎯,
    CASE WHEN all_emojis_used ilike '%🍬%' THEN 1 ELSE 0 END AS num_emoji_🍬,
    CASE WHEN all_emojis_used ilike '%🛒%' THEN 1 ELSE 0 END AS num_emoji_🛒,
    CASE WHEN all_emojis_used ilike '%💵%' THEN 1 ELSE 0 END AS num_emoji_💵,
    CASE WHEN all_emojis_used ilike '%🐥%' THEN 1 ELSE 0 END AS num_emoji_🐥,
    CASE WHEN all_emojis_used ilike '%🍫%' THEN 1 ELSE 0 END AS num_emoji_🍫,
    CASE WHEN all_emojis_used ilike '%🎄%' THEN 1 ELSE 0 END AS num_emoji_🎄,
    CASE WHEN all_emojis_used ilike '%🚬%' THEN 1 ELSE 0 END AS num_emoji_🚬,
    CASE WHEN all_emojis_used ilike '%🥥%' THEN 1 ELSE 0 END AS num_emoji_🥥,
    CASE WHEN all_emojis_used ilike '%🚗%' THEN 1 ELSE 0 END AS num_emoji_🚗,
    CASE WHEN all_emojis_used ilike '%📲%' THEN 1 ELSE 0 END AS num_emoji_📲,
    CASE WHEN all_emojis_used ilike '%🪂%' THEN 1 ELSE 0 END AS num_emoji_🪂,
    CASE WHEN all_emojis_used ilike '%🏪%' THEN 1 ELSE 0 END AS num_emoji_🏪,
    CASE WHEN all_emojis_used ilike '%🍪%' THEN 1 ELSE 0 END AS num_emoji_🍪,
    CASE WHEN all_emojis_used ilike '%👑%' THEN 1 ELSE 0 END AS num_emoji_👑,
    CASE WHEN all_emojis_used ilike '%💎%' THEN 1 ELSE 0 END AS num_emoji_💎,
    CASE WHEN all_emojis_used ilike '%🐉%' THEN 1 ELSE 0 END AS num_emoji_🐉,
    CASE WHEN all_emojis_used ilike '%🎱%' THEN 1 ELSE 0 END AS num_emoji_🎱,
    CASE WHEN all_emojis_used ilike '%💥%' THEN 1 ELSE 0 END AS num_emoji_💥,
    CASE WHEN all_emojis_used ilike '%🔥%' THEN 1 ELSE 0 END AS num_emoji_🔥,
    CASE WHEN all_emojis_used ilike '%🍀%' THEN 1 ELSE 0 END AS num_emoji_🍀,
    CASE WHEN all_emojis_used ilike '%⛽%' THEN 1 ELSE 0 END AS num_emoji_⛽,
    CASE WHEN all_emojis_used ilike '%👻%' THEN 1 ELSE 0 END AS num_emoji_👻 ,
    CASE WHEN all_emojis_used ilike '%🍇%' THEN 1 ELSE 0 END AS num_emoji_🍇,
    CASE WHEN all_emojis_used ilike '%🟢%' THEN 1 ELSE 0 END AS num_emoji_🟢,
    CASE WHEN all_emojis_used ilike '%💉%' THEN 1 ELSE 0 END AS num_emoji_💉,
    CASE WHEN all_emojis_used ilike '%🍯,%' THEN 1 ELSE 0 END AS num_emoji_🍯,
    CASE WHEN all_emojis_used ilike '%🐎%' THEN 1 ELSE 0 END AS num_emoji_🐎,
    CASE WHEN all_emojis_used ilike '%🔑%' THEN 1 ELSE 0 END AS num_emoji_🔑,
    CASE WHEN all_emojis_used ilike '%🚆%' THEN 1 ELSE 0 END AS num_emoji_🚆,
    CASE WHEN all_emojis_used ilike '%💡%' THEN 1 ELSE 0 END AS num_emoji_💡,
    CASE WHEN all_emojis_used ilike '%⚡%' THEN 1 ELSE 0 END AS num_emoji_⚡,
    CASE WHEN all_emojis_used ilike '%🍁%' THEN 1 ELSE 0 END AS num_emoji_🍁,
    CASE WHEN all_emojis_used ilike '%💰%' THEN 1 ELSE 0 END AS num_emoji_💰,
    CASE WHEN all_emojis_used ilike '%🏔%' THEN 1 ELSE 0 END AS num_emoji_🏔,
    CASE WHEN all_emojis_used ilike '%🍄%' THEN 1 ELSE 0 END AS num_emoji_🍄,
    CASE WHEN all_emojis_used ilike '%👃%' THEN 1 ELSE 0 END AS num_emoji_👃,
    CASE WHEN all_emojis_used ilike '%🌴%' THEN 1 ELSE 0 END AS num_emoji_🌴,
    CASE WHEN all_emojis_used ilike '%🅿️%' THEN 1 ELSE 0 END AS num_emoji_🅿️,
    CASE WHEN all_emojis_used ilike '%🥧%' THEN 1 ELSE 0 END AS num_emoji_🥧,
    CASE WHEN all_emojis_used ilike '%🐷%' THEN 1 ELSE 0 END AS num_emoji_🐷,
    CASE WHEN all_emojis_used ilike '%💊%' THEN 1 ELSE 0 END AS num_emoji_💊,
    CASE WHEN all_emojis_used ilike '%🔌,%' THEN 1 ELSE 0 END AS num_emoji_🔌,
    CASE WHEN all_emojis_used ilike '%📮%' THEN 1 ELSE 0 END AS num_emoji_📮,
    CASE WHEN all_emojis_used ilike '%🙏%' THEN 1 ELSE 0 END AS num_emoji_🙏,
    CASE WHEN all_emojis_used ilike '%🐀%' THEN 1 ELSE 0 END AS num_emoji_🐀,
    CASE WHEN all_emojis_used ilike '%🍚%' THEN 1 ELSE 0 END AS num_emoji_🍚,
    CASE WHEN all_emojis_used ilike '%🚀%' THEN 1 ELSE 0 END AS num_emoji_🚀,
    CASE WHEN all_emojis_used ilike '%🚌%' THEN 1 ELSE 0 END AS num_emoji_🚌,
    CASE WHEN all_emojis_used ilike '%💨%' THEN 1 ELSE 0 END AS num_emoji_💨,
    CASE WHEN all_emojis_used ilike '%🐍%' THEN 1 ELSE 0 END AS num_emoji_🐍,
    CASE WHEN all_emojis_used ilike '%🤧%' THEN 1 ELSE 0 END AS num_emoji_🤧,
    CASE WHEN all_emojis_used ilike '%❄️%' THEN 1 ELSE 0 END AS num_emoji_❄️,
    CASE WHEN all_emojis_used ilike '%🌨️%' THEN 1 ELSE 0 END AS num_emoji_🌨️,
    CASE WHEN all_emojis_used ilike '%⛄%' THEN 1 ELSE 0 END AS num_emoji_⛄,
    CASE WHEN all_emojis_used ilike '%🌲%' THEN 1 ELSE 0 END AS num_emoji_🌲,
    CASE WHEN all_emojis_used ilike '%⚪%' THEN 1 ELSE 0 END AS num_emoji_⚪,
    CASE WHEN all_emojis_used ilike '%🌿%' THEN 1 ELSE 0 END AS num_emoji_🌿,
    CASE WHEN all_emojis_used ilike '%🍃%' THEN 1 ELSE 0 END AS num_emoji_🍃,
    CASE WHEN all_emojis_used ilike '%⚗️%' THEN 1 ELSE 0 END AS num_emoji_⚗️,
    CASE WHEN all_emojis_used ilike '%🤑%' THEN 1 ELSE 0 END AS num_emoji_🤑,
    CASE WHEN all_emojis_used ilike '%🤯%' THEN 1 ELSE 0 END AS num_emoji_🤯,
    CASE WHEN all_emojis_used ilike '%🌳%' THEN 1 ELSE 0 END AS num_emoji_🌳,
    CASE WHEN all_emojis_used ilike '%🌱%' THEN 1 ELSE 0 END AS num_emoji_🌱
  FROM emoji_messages AS em
  INNER JOIN fraud_deletes AS fd ON fd.member_profile_id = em.profile_id
  GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
  ORDER BY profile_id DESC
)
--> Main select query
SELECT
  fraud_delete_reason,
  --> Total number of messages from fraud deleted members for each fraud delete reason
  COUNT(fraud_delete_reason) AS num_fraud_deleted_messages,
  --> Number of times each emoji was used in a a message from a fraud deleted member sorted by fraud deleted reason
    SUM(num_emoji_😈) AS num_emoji_😈,
    SUM(num_emoji_💯 ) AS num_emoji_💯,
    SUM(num_emoji_🍌) AS num_emoji_🍌,
    SUM(num_emoji_🔵) AS num_emoji_🔵,
    SUM(num_emoji_💣) AS num_emoji_💣,
    SUM(num_emoji_📦) AS num_emoji_📦,
    SUM(num_emoji_🥦) AS num_emoji_🥦,
    SUM(num_emoji_🎯) AS num_emoji_🎯,
    SUM(num_emoji_🍬) AS num_emoji_🍬,
    SUM(num_emoji_🚗 ) AS num_emoji_🚗,
    SUM(num_emoji_🛒) AS num_emoji_🛒,
    SUM(num_emoji_💵) AS num_emoji_💵,
    SUM(num_emoji_🐥) AS num_emoji_🐥,
    SUM(num_emoji_🍫) AS num_emoji_🍫,
    SUM(num_emoji_🎄) AS num_emoji_🎄,
    SUM(num_emoji_🚬) AS num_emoji_🚬,
    SUM(num_emoji_🥥) AS num_emoji_🥥,
    SUM(num_emoji_📲) AS num_emoji_📲,
    SUM(num_emoji_🪂) AS num_emoji_🪂,
    SUM(num_emoji_🏪 ) AS num_emoji_🏪,
    SUM(num_emoji_🍪 ) AS num_emoji_🍪,
    SUM(num_emoji_👑) AS num_emoji_👑,
    SUM(num_emoji_💎 ) AS num_emoji_💎,
    SUM(num_emoji_🐉) AS num_emoji_🐉,
    SUM(num_emoji_🎱) AS num_emoji_🎱,
    SUM(num_emoji_💥) AS num_emoji_💥,
    SUM(num_emoji_🔥) AS num_emoji_🔥,
    SUM(num_emoji_🍀) AS num_emoji_🍀,
    SUM(num_emoji_⛽) AS num_emoji_⛽,
    SUM(num_emoji_👻 ) AS num_emoji_👻,
    SUM(num_emoji_🍇) AS num_emoji_🍇,
    SUM(num_emoji_🟢) AS num_emoji_🟢,
    SUM(num_emoji_💉) AS num_emoji_💉,
    SUM(num_emoji_🍯 ) AS num_emoji_🍯,
    SUM(num_emoji_🐎) AS num_emoji_🐎,
    SUM(num_emoji_🔑) AS num_emoji_🔑,
    SUM(num_emoji_🚆) AS num_emoji_🚆,
    SUM(num_emoji_💡) AS num_emoji_💡,
    SUM(num_emoji_⚡) AS num_emoji_⚡,
    SUM(num_emoji_🍁) AS num_emoji_🍁,
    SUM(num_emoji_💰) AS num_emoji_💰,
    SUM(num_emoji_🏔) AS num_emoji_🏔,
    SUM(num_emoji_🍄) AS num_emoji_🍄,
    SUM(num_emoji_👃) AS num_emoji_👃,
    SUM(num_emoji_🌴) AS num_emoji_🌴,
    SUM(num_emoji_🅿️) AS num_emoji_🅿️,
    SUM(num_emoji_🥧 ) AS num_emoji_🥧,
    SUM(num_emoji_🐷 ) AS num_emoji_🐷,
    SUM(num_emoji_💊) AS num_emoji_💊,
    SUM(num_emoji_🔌 ) AS num_emoji_🔌,
    SUM(num_emoji_📮) AS num_emoji_📮,
    SUM(num_emoji_🙏 ) AS num_emoji_🙏,
    SUM(num_emoji_🐀 ) AS num_emoji_🐀,
    SUM(num_emoji_🍚) AS num_emoji_🍚,
    SUM(num_emoji_🚀) AS num_emoji_🚀,
    SUM(num_emoji_🚌) AS num_emoji_🚌,
    SUM(num_emoji_💨) AS num_emoji_💨,
    SUM(num_emoji_🐍) AS num_emoji_🐍,
    SUM(num_emoji_🤧) AS num_emoji_🤧,
    SUM(num_emoji_❄️) AS num_emoji_❄️,
    SUM(num_emoji_🌨️) AS num_emoji_🌨️,
    SUM(num_emoji_⛄) AS num_emoji_⛄,
    SUM(num_emoji_🌲) AS num_emoji_🌲,
    SUM(num_emoji_⚪) AS num_emoji_⚪,
    SUM(num_emoji_🌿) AS num_emoji_🌿,
    SUM(num_emoji_🍃) AS num_emoji_🍃,
    SUM(num_emoji_⚗️) AS num_emoji_⚗️,
    SUM(num_emoji_🤑) AS num_emoji_🤑,
    SUM(num_emoji_🤯) AS num_emoji_🤯,
    SUM(num_emoji_🌳) AS num_emoji_🌳,
    SUM(num_emoji_🌱) AS num_emoji_🌱
FROM emojis_used
GROUP BY 1
ORDER BY 2 DESC