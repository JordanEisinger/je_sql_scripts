# JIRA Ticket Queries

* *Author*: Jordan Eisinger, Data Scientist
* *Description*: SQL queries for open, in progress, and closed JIRA tickets
* *Other Notes*:
  * 'DST_####' refers to JIRA ticket number under Data Science Team (DST) project
  * Text after ticket number is a brief description of query purpose and context

## Script notes section template

    /* ===================================================================================
    As of: INSERT UPDATED AT HERE

    General Notes:
    -Section for any notes

    URLs:
    -Websites or other resoruce links

    Changelog:
    -Date of Change: What was changed
    =================================================================================== */
