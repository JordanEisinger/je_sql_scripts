/* ===================================================================================
As of: 09/25/2023

General Notes:
-Query ran for past year, results put into a Google Sheet 

Changelog:
09/25/23: Added query to repo and ticket
=================================================================================== */

WITH flagged_payments AS (
  SELECT
    *
  FROM u2p_sites.payments
  WHERE 1=1
    AND status IN ('chargeback', 'declined', 'refunded', 'REFUNDED')
    AND created_at >= GETDATE() - INTERVAL '365 day'
--     AND id IN ('10266613', '10266626')
)

, alaska_fraud AS (
SELECT 
  * 
  ,udf.decr(request_data, 'U2P') AS decr_request_data
  ,JSON_EXTRACT_PATH_TEXT(decr_request_data, 'address_city', true) AS card_city
  ,JSON_EXTRACT_PATH_TEXT(decr_request_data, 'address_state', true) AS card_state
FROM flagged_payments
WHERE 1=1
  AND card_city ilike '%juneau%'
LIMIT 100
)

SELECT 
  p.id AS profile_id
  ,p.user_id
  ,p.is_fraud
  ,p.created_at AS profile_created_at
  ,af.id AS payment_id
  ,af.created_at AS payment_created_at
  ,l.name AS profile_join_location
  ,af.card_city AS billing_card_city
  ,af.card_state AS billing_card_state
FROM alaska_fraud AS af
INNER JOIN u2p_sites.customers AS c ON c.id = af.customer_id
INNER JOIN seeking_arrange.profiles AS p ON p.user_id = c.account_id
INNEr JOIN seeking_arrange.locations AS l ON l.profile_id = p.id