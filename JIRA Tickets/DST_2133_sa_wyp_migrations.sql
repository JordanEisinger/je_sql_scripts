/* ===================================================================================
As of: 12/29/2022

General Notes:
-Start with CTE of ported emails in WYP (~18K) Need to decrypt, but table small enough for it not to be a problem
-Get SA accounts that clicked through email
-Decrypt email on WYP and SA side to match
-Add timestamp for when opened email and when ported account
-Digi Acoustic Events event_type = Click Through
-SA to WYP landing page URL -> refid = Seeking uid -> no associated id in WYP, but may be able to use to narrow down Seeking list
-to those who clicked the migration email link
-Check cloudflare logs for URL refid (Kibana)
-Change from subsequent CTE's for first 3 and make one query with INNER JOIN on subqueries
-Inner most sub query: users, profiles, cloudflare, wyp_migrations
-Include acoustic events for total sends

Changelog:
07/14/22: Query now joins SA to WYP migated accounts on email. Need to validate timestamps 
          for each to get accurate click through date
07/19/22: Changed query structure to use sub-queries vs. CTE's
08/02/22: Added acoustic events query for sends, open, click-through info
08/22/22: Aggregated by email sent date with total counts
12/29/22: Re-structured query based on logic for Dashboard 590
=================================================================================== */

--> Start with CTE of total account migrations at the email level
WITH total_migrations AS (
SELECT
  derived_message_id AS id,
  DATE(sent_at) AS sent_at_date,
  SUM(is_migrated) AS total_migrated
FROM (
SELECT
  ae.*, e.*, am.*,
  CASE WHEN wyp_migration_created_at IS NOT NULL THEN 1 ELSE 0 END AS is_migrated
FROM (
  --> Decrypt Seeking emails
  SELECT
    sa_profile_id, sa_user_id, sa_uid, UDF.DECR(email,'SA') AS sa_email, cf_timestamp
  FROM (
    --> Get Seeking users user_id and email
    SELECT
      p.id AS sa_profile_id, p.user_id AS sa_user_id, p.uid AS sa_uid, u.email, cf_timestamp
    FROM (
      --> Get Seeking uid of only users that clicked through migration email
      SELECT
        SUBSTRING(REGEXP_SUBSTR(clientrequesturi,'refid=[^&]*'),7) AS seeking_uid,
        MAX(edgestarttimestamp) AS cf_timestamp
      FROM system_logs.sa_cloudflare_spectrum
      WHERE 1=1
        AND ts_index > getdate() - interval '30 day'
        AND clientrequestpath ilike '%oneclick/wyp%'
        AND clientrequesturi ilike '%oneclick/wyp%'
      GROUP BY 1
    ) AS cf 
    INNER JOIN seeking_arrange.profiles AS p ON cf.seeking_uid = p.uid
    INNER JOIN seeking_arrange.users AS u ON u.id = p.user_id
  ) 
) AS e
--> WYP account migrations
LEFT JOIN (
  SELECT
    account_id,
    UDF.DECR(email,'WYP') AS wyp_email,
    created_at AS wyp_migration_created_at,
    updated_at AS wyp_migration_updated_at
  FROM (
    SELECT 
      account_id,
      JSON_EXTRACT_PATH_TEXT(payload,'request','email') AS email,
      created_at,
      updated_at
    FROM wyp.account_migrations AS wyp
    --> WHERE clause for testing only so don't have to decrypt 18K+ emails every query run
    WHERE 1=1
      AND created_at > GETDATE() - interval '30 day'
  )
) AS am ON am.wyp_email = e.sa_email
--> Acoustic Events emails sent
LEFT JOIN (
SELECT
  recipient_id || mailing_id || report_id AS derived_message_id,
  recipient_id, account_id, acoustic_list_name, mailing_name, mailing_subject, program_id, campaign_id, content_id, report_id,
  MAX(CASE WHEN event_type = 'Sent' THEN event_timestamp END) AS sent_at,
  CASE WHEN (sent_at IS NOT NULL) THEN derived_message_id ELSE NULL END AS is_sent
FROM digital_marketing.acoustic_events
WHERE 1=1
  AND event_timestamp > GETDATE() - interval '30 day'
  AND program_id = '268766'
GROUP BY 1,2,3,4,5,6,7,8,9,10
) AS ae ON ae.account_id = e.sa_user_id
)
WHERE 1=1
  AND sent_at_date IS NOT NULL
GROUP BY 1,2
ORDER BY sent_at_date DESC
)
--> Acoustic Events logic based on Dashboard 590
SELECT
    acoustic_list_name,
    mailing_subject,
    mailing_name,
    program_id,
    a.sent_at_date,
    COUNT(DISTINCT is_sent) AS total_sent,
    COUNT(DISTINCT is_opened) AS total_opened,
    COUNT(DISTINCT is_clicked) AS total_clicked,
    SUM(total_migrated) AS total_migrated
FROM (
  SELECT
    derived_message_id, acoustic_list_name, mailing_subject, mailing_name, program_id,
    DATE(CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', sent_at)) AS sent_at_date,
    is_sent,
    is_opened,
    is_clicked
  FROM (
    --> Logic from LookML for Dashboard 590
    SELECT
      recipient_id || mailing_id || report_id AS derived_message_id,
      recipient_id, account_id, acoustic_list_name, mailing_name, mailing_subject, program_id, campaign_id, content_id, report_id,
      MIN(event_timestamp)::timestamp AS min_event_timestamp,
      MAX(event_timestamp)::timestamp AS max_event_timestamp,
      MAX(CASE WHEN event_type = 'Sent' THEN event_timestamp END)::timestamp AS sent_at,
      MAX(CASE WHEN event_type = 'Open' THEN event_timestamp END)::timestamp AS opened_at,
      MAX(CASE WHEN event_type = 'Click Through' THEN event_timestamp END)::timestamp AS clicked_through_at,
      MAX(CASE WHEN event_type = 'Conversion' THEN event_timestamp END)::timestamp AS conversion_at,
      MAX(CASE WHEN event_type = 'Opt Out' THEN event_timestamp END)::timestamp AS opted_out_at,
      MAX(CASE WHEN event_type = 'Suppressed' THEN event_timestamp END)::timestamp AS suppressed_at,
      MAX(CASE WHEN event_type = 'Soft Bounce' THEN event_timestamp END)::timestamp AS soft_bounced_at,
      MAX(CASE WHEN event_type = 'Hard Bounce' THEN event_timestamp END)::timestamp AS hard_bounced_at,
      MAX(CASE WHEN event_type = 'Reply Mail Block' THEN event_timestamp END)::timestamp AS reply_mail_blocked_at,
      MAX(CASE WHEN event_type = 'Reply Abuse' THEN event_timestamp END)::timestamp AS reply_abuse_at,
      MAX(CASE WHEN event_type = 'Reply Other' THEN event_timestamp END) AS reply_other_at,
      MAX(CASE WHEN event_type = 'Reply Mail Restriction' THEN event_timestamp END) AS reply_mail_restriction_at,
      CASE WHEN (sent_at IS NOT NULL) THEN derived_message_id ELSE NULL END AS is_sent,
      CASE WHEN (opened_at IS NOT NULL) THEN derived_message_id ELSE NULL END AS is_opened,
      CASE WHEN (clicked_through_at IS NOT NULL) THEN derived_message_id ELSE NULL END AS is_clicked
    FROM digital_marketing.acoustic_events
    WHERE 1=1
      AND event_timestamp > GETDATE() - interval '30 day'
      AND program_id = '268766'
    GROUP BY 1,2,3,4,5,6,7,8,9,10
  )
  GROUP BY 1,2,3,4,5,6,7,8,9
) AS a
--> join to CTE to get migrations
LEFT JOIN total_migrations AS tm ON tm.id = a.derived_message_id
WHERE 1=1
  --> Only emails that werre sent
  AND a.sent_at_date IS NOT NULL
GROUP BY 1,2,3,4,5
ORDER BY a.sent_at_date DESC
