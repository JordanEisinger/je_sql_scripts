/* Queries for DST_1928 */

/* 
Noted date is before the minumum date in rollup.sa_session_tracking table
Need to do fuzzy search instead in the following tables for profile activity:
  messages -- Min created at is 2019
  threads -- 1 profile found for 04/21/2017
  favorites -- 4 profilies found, but none in April 2017
  payments / paymentlogs 
  apilogs -- 2 accounts active in April, but not on noted date
  profilehistory - Records for profiles, but none around date
*/

-- CTE's
WITH af_profile_list AS (
  SELECT
    C1.*,
    COALESCE(datediff('day', C1.reported_dob, C1.profile_created_at::date) / 365, -1) AS reported_join_age,
    COALESCE(datediff('day', C1.reported_dob, '2017-04-24') / 365, -1) AS reported_activiy_age,
    COALESCE(datediff('day', C1.reported_dob, getdate()::date) / 365, -1) AS reported_current_age
  FROM (
    SELECT
      p.id AS profile_id,
      p.uid,
      p.user_id,
      p.created_at AS profile_created_at,
      to_date(
        (max(case when pav.profileattribute_id = 20 then pav.name else null end))||' '||
        (max(case when pav.profileattribute_id = 21 then pav.name else null end))||', '||
        (max(case when pav.profileattribute_id = 22 then pav.name else null end)), 'month DD, YYYY'
      ) as reported_dob,
      l.city AS reported_city,
      l.region AS reported_region
    FROM seeking_arrange.profiles AS p
    LEFT JOIN seeking_arrange.locations AS l ON l.profile_id = p.id
    left join seeking_arrange.profile_profileattributevalue as ppav on ppav.profile_id = p.id
    left join seeking_arrange.profileattributevalues as pav on ppav.profileattributevalue_id = pav.id
          and pav.profileattribute_id in (20, 21, 22)
    WHERE 1=1
      AND p.created_at <= '2017-04-30T00:00:00.000+00:00' -- narrows down list of profiles to only those that could have been active on date
      AND p.account_type = 'attractive'
      AND p.sex = 'Female'
      AND l.city = 'Orange County'
      AND l.region = 'Florida'
    GROUP BY 1,2,3,4,6,7
  ) AS C1
),
thread_list AS (
SELECT
  t.id as thread_id,
  t.profile_id_1,
  t.profile_id_2,
  t.created_at
FROM seeking_arrange.threads AS t 
WHERE 1=1
  AND t.created_at BETWEEN '2017-04-01T00:00:00.000+00:00' AND '2017-04-30T00:00:00.000+00:00'
)

-- List of profiles
SELECT * FROM af_profile_list

-- Profiles with message/thread activity:
SELECT 
  tl.*,
  afpl.profile_id
FROM thread_list AS tl
LEFT JOIN af_profile_list AS afpl ON afpl.profile_id = tl.profile_id_1
WHERE 1=1
  AND tl.profile_id_1 IN (afpl.profile_id)
UNION
SELECT 
  tl.*,
  afpl.profile_id
FROM thread_list AS tl
LEFT JOIN af_profile_list AS afpl ON afpl.profile_id = tl.profile_id_2
WHERE 1=1
  AND tl.profile_id_2 IN (afpl.profile_id)
  
-- Profiles with favorites activity:
SELECT 
  f.*,
  afpl.profile_id AS list_profile_id
FROM seeking_arrange.favorites AS f
LEFT JOIN af_profile_list AS afpl ON afpl.profile_id = f.profile_id
WHERE 1=1
  AND f.profile_id IN (afpl.profile_id)
AND f.created_at BETWEEN '2017-04-01T00:00:00.000+00:00' AND '2017-04-30T00:00:00.000+00:00'
AND f.updated_at BETWEEN '2017-04-01T00:00:00.000+00:00' AND '2017-04-30T00:00:00.000+00:00' 
AND f.deleted_at BETWEEN '2017-04-01T00:00:00.000+00:00' AND '2017-04-30T00:00:00.000+00:00'
LIMIT 10

-- Profiles with profile history activity:
SELECT
  ph.id, ph.profile_id, ph.user_id, pht.type, pht.description, ph.value, ph.created_at, ph.updated_at
FROM seeking_arrange.profilehistory AS ph
LEFT JOIN seeking_arrange.profilehistorytypes AS pht ON pht.id = ph.history_type_id
LEFT JOIN af_profile_list AS afpl ON afpl.profile_id = ph.profile_id
WHERE 1=1
  AND ph.profile_id IN (afpl.profile_id)
  AND ph.created_at BETWEEN '2017-04-01T00:00:00.000+00:00' AND '2017-04-30T00:00:00.000+00:00'
  OR (ph.updated_at BETWEEN '2017-04-01T00:00:00.000+00:00' AND '2017-04-30T00:00:00.000+00:00')
ORDER BY profile_id ASC

-- Profiles with activity in apilogs
SELECT
  afpl.profile_id, al.*
FROM seeking_arrange.apilogs AS al
LEFT JOIN seeking_arrange.apis AS a ON a.api_key = al.api_key
LEFT JOIN af_profile_list AS afpl ON afpl.profile_id = a.id
WHERE 1=1
AND al.api_key IN (
  SELECT 
    api_key 
  FROM seeking_arrange.apis 
  WHERE id IN(SELECT profile_id FROM af_profile_list)
  )
AND al.created_at BETWEEN '2017-04-01T00:00:00.000+00:00' AND '2017-04-30T00:00:00.000+00:00'