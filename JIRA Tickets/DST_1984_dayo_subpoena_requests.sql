/* ===================================================================================
As of: 03/16/2022

General Notes:
-Full datamine for profiles of interest in every Dayo table
-Need to automate items in WHERE clause (try temp table or CTE)

Changelog:
=================================================================================== */

-- Users
SELECT * FROM dayo.users 
WHERE 1=1
  AND uid IN ('96cf5d89-8f1f-4647-a9ff-5cb419df6faa', '8ff12bf9-69d8-4222-b639-b58f686f78b4')
LIMIT 10

-- Profiles
SELECT * FROM dayo.profiles 
WHERE 1=1
  AND user_id IN (24, 247)
LIMIT 10

-- Access Logs
SELECT * FROM dayo.access_logs
WHERE 1=1
  AND user_id IN (24, 247)
ORDER BY created_at ASC

-- Activity Events
SELECT * FROM dayo.activity_events 
WHERE 1=1
  AND user_id IN (24, 247)
ORDER BY created_at ASC

-- Admin Audit Logs
SELECT * FROM dayo.admin_audit_logs 
WHERE 1=1
   AND user_id IN (24, 247)
ORDER BY created_at ASC

-- Admin Notes
SELECT * FROM dayo.admin_notes 
WHERE 1=1
   AND user_id IN (24, 247)
   
-- Crushes
SELECT * FROM dayo.crushes
WHERE 1=1
   AND (user_id IN (24, 247) OR member_id IN (24, 247))
ORDER BY created_at ASC

-- Email verifications
SELECT * FROM dayo.email_verifications 
WHERE 1=1
   AND user_id IN (24, 247)
   
-- Feed Settings
SELECT * FROM dayo.feed_settings
WHERE 1=1
   AND user_id IN (24, 247)
   
-- Medias
SELECT * FROM dayo.medias
WHERE 1=1
   AND user_id IN (24, 247)
ORDER BY created_at ASC

-- Post views
SELECT * FROM dayo.post_views
WHERE 1=1
   AND (user_id IN (24, 247) OR member_id IN (24, 247))
ORDER BY created_at ASC

-- Posts
SELECT * FROM dayo.posts
WHERE 1=1
   AND user_id IN (24, 247)
ORDER BY created_at ASC

-- Post activities
SELECT * FROM dayo.posts_activities
WHERE 1=1
   AND user_id IN (24, 247)
ORDER BY created_at ASC

-- Profile Views
SELECT * FROM dayo.profile_views
WHERE 1=1
   AND (user_id IN (24, 247) OR member_id IN (24, 247))
ORDER BY created_at ASC

-- Reports
SELECT * FROM dayo.reports 
WHERE 1=1
   AND (user_id IN (24, 247) OR member_id IN (24, 247))
ORDER BY created_at ASC

-- Video Calls
SELECT * FROM dayo.video_calls
WHERE 1=1
   AND (from_user_id IN (24, 247) OR to_user_id IN (24, 247))
ORDER BY created_at ASC