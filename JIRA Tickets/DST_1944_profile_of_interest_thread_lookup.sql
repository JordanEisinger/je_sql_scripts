/* ===================================================================================
As of: 03/02/2022

General Notes:
-Queries pull all thread records for a noted profile of interest and a full list
  of profiles they interacted with
-Includes profile id, uid, email, username

Changelog:
=================================================================================== */

-- Profile of interest
SELECT * FROM seeking_arrange.profiles WHERE id = 7596854

-- Query 1
WITH threads_of_interest AS (
  SELECT
    profile_id_2 AS profile_of_interest,
    created_at AS thread_created_at,
    last_message_from_me_at_1 AS most_recent_message_sent,
    last_message_from_me_at_2 AS most_recent_message_sent_by_other
  FROM seeking_arrange.threads
  WHERE profile_id_1  = 7596854
  UNION
  SELECT  
    profile_id_1 AS profile_of_interest,
    created_at AS thread_created_at,
    last_message_from_me_at_2 AS most_recent_message_sent,
    last_message_from_me_at_1 AS most_recent_message_sent_by_other
  FROM seeking_arrange.threads
  WHERE profile_id_2 = 7596854
),
profile_list AS (
  SELECT 
    p.id AS profile_id, p.uid AS profile_uid, pp.id AS pasprofile_id, udf.decr(u.email, 'SA')::varchar(384) AS email,
    t.thread_created_at, t.most_recent_message_sent, t.most_recent_message_sent_by_other  
  FROM seeking_arrange.profiles AS p
  INNER JOIN threads_of_interest AS t ON t.profile_of_interest = p.id
  LEFT JOIN seeking_arrange.pasprofiles AS pp ON pp.profile_key = p.uid
  LEFT JOIN seeking_arrange.users AS u ON u.id = p.user_id
),
username_list AS (
  SELECT
    pasprofile_id, JSON_EXTRACT_PATH_TEXT(pc.content_data, 'data', 'text', 0, 'value', true) AS username
  FROM seeking_arrange.pascontents AS pc
  WHERE 1=1
    AND JSON_EXTRACT_PATH_TEXT(pc.content_data, 'data', 'text', 0, 'label', true) = 'Username' 
)
SELECT * 
FROM profile_list AS pl
LEFT JOIN username_list AS ul ON ul.pasprofile_id = pl.pasprofile_id
ORDER BY profile_id ASC