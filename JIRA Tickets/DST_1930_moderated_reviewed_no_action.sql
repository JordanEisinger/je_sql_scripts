/* ===================================================================================
As of: 02/21/2022

General Notes:
-3 messages were autoflagged for flagged keywords like 'under 18', 'virginity', etc.
-Message content in PAS queue were marked at 'Reviewed, No Action' by moderators
-Marked as messageflag.status = NOTPROBLEM in database

Changelog:
=================================================================================== */

-- Auto flagged messages with status and moderator
SELECT 
  mf.*, 
  pkw.keyword, 
  pkw.description 
FROM seeking_arrange.messageflags AS mf
LEFT JOIN seeking_arrange.profileflagkeywords pkw ON pkw.id = mf.flagkeyword_id
WHERE 1=1
  AND profile_id IN ('4313221', '39296560')
-- NOTPROBLEM => 'REVIEWED WITH NO ACTION'

-- Auto flagged messages
SELECT m.* 
FROM seeking_arrange.messages AS m
WHERE 1=1
  AND m.id IN (
    SELECT message_id 
    FROM seeking_arrange.messageflags 
    WHERE 1=1
      AND profile_id IN ('4313221', '39296560')
  )

-- Moderators who 'Reviewed With No Action' on above auto flagged messages
SELECT * FROM seeking_arrange.admins
WHERE 1=1
  AND user_id IN ('4958441','5693766')

/* ======================== */
/* OTHER QUERIES FOR TICKET */
/* ======================== */

-- SA Profiles records
SELECT * FROM seeking_arrange.profiles
WHERE 1=1
  AND uid IN ('30664f6c-9275-04c7-7323-5baa07c73ddb', 'bd8158b0-3882-4f64-9f65-4ab298406c6f')

-- PAS profile records
SELECT * FROM seeking_arrange.pasprofiles 
WHERE 1=1
  AND profile_key IN ('30664f6c-9275-04c7-7323-5baa07c73ddb', 'bd8158b0-3882-4f64-9f65-4ab298406c6f')

-- All messages between two profiles of interest around noted date
SELECT * FROM seeking_arrange.messages
WHERE 1=1
  AND profile_id IN ('4313221', '39296560')
  -- AND body ilike '%18%'
  AND created_at BETWEEN '2022-01-12T00:00:00.000+00:00' AND '2022-01-13T00:00:00.000+00:00'
ORDER BY created_at DESC
LIMIT 750 

-- Admin Notes records for profiles
SELECT * FROM seeking_arrange.adminnotes 
WHERE profile_id IN ('4313221', '39296560')
ORDER BY profile_ID ASC, created_at DESC

-- PAS moderations records for profiles
SELECT 
  *,
  JSON_EXTRACT_PATH_TEXT(pasmoderations.extended_data, 'data',0,'old_value',true) as extracted_text
FROM seeking_arrange.pasmoderations 
WHERE 1=1
  AND pasprofile_id IN ('35140718','8405')
  and extracted_text ilike '%18%'
ORDER BY created_at DESC
LIMIT 100