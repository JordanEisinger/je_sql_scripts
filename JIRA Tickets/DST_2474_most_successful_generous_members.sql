/* ===================================================================================
As of: 11/16/2022

General Notes:
-Scripts for generous males sorted by revenue in past 13 months
-As of 11/2022, ~333K total paying members in last 13 months, with ~240K with total revenue > $0

Changelog:
11/16/22: Added comments to query
=================================================================================== */

SELECT
  ROW_NUMBER() OVER (ORDER BY total_revenue DESC) AS row_num,
  p.id AS profile_id,
  (CASE WHEN p.is_diamond_member = 1  THEN 'Yes' ELSE 'No' END) AS diamond_member,
  SUM(pl.amount_usd ) AS total_revenue
FROM seeking_arrange.profiles AS p
LEFT JOIN seeking_arrange.payments AS pp ON p.id = pp.billable_id
LEFT JOIN seeking_arrange.paymentlogs AS pl ON pp.id = pl.payment_id
WHERE 1=1
  --> Remove test accounts
  AND profile_id NOT IN ('10096715')
  --> Only generous accounts
  AND p.account_type = 'generous' 
  --> Last 13 months
  AND ((( pl.created_at::timestamp  ) >= 
    ((CONVERT_TIMEZONE('America/Los_Angeles', 'UTC', DATEADD(month,-12, DATE_TRUNC('month', DATE_TRUNC('day',CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', GETDATE()))) )))) 
    AND ( pl.created_at::timestamp  ) < ((CONVERT_TIMEZONE('America/Los_Angeles', 'UTC', DATEADD(month,13, DATEADD(month,-12, DATE_TRUNC('month', DATE_TRUNC('day',CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', GETDATE()))) ) ))))))
GROUP BY 2,3
HAVING 
  --> Only return users who paid
  total_revenue > 0
ORDER BY 4 DESC
