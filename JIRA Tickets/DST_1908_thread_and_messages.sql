/* ===================================================================================
As of: 07/26/2022 

General Notes:
-Shows relationship between the 'seeking_arrange.messages' and 'seeking_arrange.threads' tables

Changelog:
=================================================================================== */

--- CASE 1: 6 profiles are profile_id_1 ---
WITH message_list_1 AS (
SELECT id, profile_id_1, profile_id_2
FROM seeking_arrange.threads 
WHERE 1=1
  AND profile_id_1 in ('10150526', '39134181', '35146032', '2567883', '2567883', '39233583', '685281')
),
common_messaged_list AS (
SELECT profile_id_2, COUNT(profile_id_2) as count
FROM message_list_1
GROUP BY profile_id_2 HAVING count > 1
ORDER BY count DESC
),
common_thread_id AS (
SELECT ml1.id, ml1.profile_id_1, ml1.profile_id_2
FROM message_list_1 AS ml1
INNER JOIN common_messaged_list AS cml ON cml.profile_id_2 = ml1.profile_id_2
)
SELECT 
  m.conversation_id AS thread_id,  
  m.id AS message_id, 
  m.profile_id AS sender_profile_id, 
  CASE 
    WHEN m.profile_id = cti.profile_id_1 THEN cti.profile_id_2
    ELSE cti.profile_id_1
  END AS receiver_profile_id,
  m.created_at AS message_sent_date,
  m.body as message_body
FROM seeking_arrange.messages AS m
INNER JOIN common_thread_id AS cti ON cti.id = m.conversation_id
ORDER BY thread_id DESC, message_sent_date ASC


--- CASE 2: 6 profiles are profile_id_2 ---
WITH message_list_2 AS (
SELECT id, profile_id_1, profile_id_2
FROM seeking_arrange.threads 
WHERE 1=1
  AND profile_id_2 in ('10150526', '39134181', '35146032', '2567883', '2567883', '39233583', '685281')
),
common_messaged_list AS (
SELECT profile_id_1, COUNT(profile_id_1) as count
FROM message_list_2
GROUP BY profile_id_1 HAVING count > 1
ORDER BY count DESC
),
common_thread_id AS (
SELECT ml2.id, ml2.profile_id_1, ml2.profile_id_2
FROM message_list_2 AS ml2
INNER JOIN common_messaged_list AS cml ON cml.profile_id_1 = ml2.profile_id_1
)
SELECT 
  m.conversation_id AS thread_id,  
  m.id AS message_id, 
  m.profile_id AS sender_profile_id, 
  CASE 
    WHEN m.profile_id = cti.profile_id_1 THEN cti.profile_id_2
    ELSE cti.profile_id_1
  END AS receiver_profile_id,
  m.created_at AS message_sent_date,
  m.body as message_body
FROM seeking_arrange.messages AS m
INNER JOIN common_thread_id AS cti ON cti.id = m.conversation_id
ORDER BY thread_id DESC, message_sent_date ASC