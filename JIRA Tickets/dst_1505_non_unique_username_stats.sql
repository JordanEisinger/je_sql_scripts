SELECT
  COUNT(username),
  created_at
FROM(
SELECT
  username,
  COUNT(username) num_username,
  created_at
FROM(
SELECT
  LOWER(JSON_EXTRACT_PATH_TEXT(profiles.profile_attributes, 'username',0,'value',true)) as username,
  DATE(CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', profiles.created_at::timestamp )) AS created_at
FROM seeking_arrange.profiles 
WHERE 1=1 
  AND LENGTH(JSON_EXTRACT_PATH_TEXT(profiles.profile_attributes, 'username', 0, 'value', true) ) <> 0
  AND (JSON_EXTRACT_PATH_TEXT(profiles.profile_attributes, 'username', 0, 'value', true) ) IS NOT NULL
  AND created_at >= '2021-06-01T00:00:00.000+00:00'
  -- AND username like 'j%'
-- LIMIT 100
)
GROUP BY 1,3
-- HAVING num_username > 1
ORDER BY num_username DESC
)
GROUP BY 2
ORDER BY created_at DESC
