--> How many messages does a Male/Female member receive in the first week of creating a profile
WITH messages AS (
SELECT
  t.profile_id_2 AS profile_id,
  COUNT(profile_id) AS count,
  CASE 
    WHEN p.account_type = 'generous' AND sex = 'Male' THEN 'GM4F' 
    WHEN account_type = 'attractive' AND sex = 'Female' THEN 'AF4M' 
    ELSE 'Other' 
  END AS account_type
FROM seeking_arrange.threads AS t
LEFT JOIN seeking_arrange.profiles AS p on p.id = t.profile_id_2
WHERE 1=1
  AND last_approved_message_for_me_at_2 BETWEEN p.created_at AND p.created_at+7
GROUP BY 1,3
)
SELECT
  account_type,
  AVG(count)
FROM messages
GROUP BY 1

SELECT * FROM seeking_arrange.messages 
WHERE 1=1
  AND conversation_id = 789954327

SELECT * FROM seeking_arrange.profiles WHERE id = 22804253
