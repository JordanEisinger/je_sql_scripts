/* ===================================================================================
As of: 01/03/2023

General Notes:
-Initially created as part of DST-2197
-Part 1 of ticket is regarding which 'Search More' link/button is used more by member type
-Part 2 of ticket is regarding specific search filter usage

Changelog:
11/03/22: Created initial query
11/16/22: Added query for testing search filters with a test account userid
01/02/23: Updated Part 1 query, added Part 2 query
=================================================================================== */

--> Part 1: 'Search More' basic analytics
WITH vysion_data AS (
  SELECT 
    *,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'gender',true) AS gender,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'sexual_preference',true) AS sexual_preference,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'account_type',true) AS account_type,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'member_level',true) AS member_level
  FROM vysion.vysion_logs AS vl
  WHERE 1=1
    --> Include timestamp index to refine search
    AND ts_index > GETDATE() - interval '30 day'
    --> Only for Seeking.com
    AND platform = 'sa'
    --> Specific eventnames forr clicking 'Search More' link and through profile picture
    AND SPLIT_PART(eventname,'-',3) IN ('searchMore', 'profilePhoto')
)
SELECT
  v.clientsessionid, v.userid, v.eventname, 
  SPLIT_PART(v.eventname,'-',2) AS panel_name,
  SPLIT_PART(v.eventname,'-',3) AS link_name,
  v.server_ts,
  -- CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', v.server_ts) AS server_ts_pst, 
  -- DATE(server_ts_pst) AS date_pst,
  v.member_level,
  CASE
    WHEN gender = 'male' AND sexual_preference = 'female' AND account_type = 'generous' THEN 'GM4F'
    WHEN gender = 'female' AND sexual_preference = 'male' AND account_type = 'attractive' THEN 'AF4M'
    ELSE 'Other'
  END AS member_type
FROM vysion_data AS v
ORDER BY date_pst DESC
LIMIT 100

--> Part 2: Search filter options basic analytics
WITH vysion_data AS (
  SELECT
    *,
    JSON_EXTRACT_PATH_TEXT(vl.eventdata, 'url', true) AS search_url,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'gender',true) AS gender,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'sexual_preference',true) AS sexual_preference,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'account_type',true) AS account_type,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'member_level',true) AS member_level
  FROM vysion.vysion_logs AS vl
  WHERE 1=1
    --> Include timestamp index to refine search
    AND ts_index > GETDATE() - interval '30 day'
    --> Only for Seeking
    AND platform = 'sa'
    --> Specific eventname for searches
    AND LOWER(eventname) = 'search'
)
SELECT
  v.clientsessionid, v.userid, v.eventname, v.search_url, v.server_ts, v.member_level,
  CASE
    WHEN gender = 'male' AND sexual_preference = 'female' AND account_type = 'generous' THEN 'GM4F'
    WHEN gender = 'female' AND sexual_preference = 'male' AND account_type = 'attractive' THEN 'AF4M'
    ELSE 'Other'
  END AS member_type
FROM vysion_data AS v
ORDER BY server_ts DESC
LIMIT 100

--> Part 3: Blog click throughs
WITH vysion_data AS (
  SELECT
    *,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'gender',true) AS gender,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'sexual_preference',true) AS sexual_preference,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'account_type',true) AS account_type,
    JSON_EXTRACT_PATH_TEXT(vl.userdata,'member_level',true) AS member_level
  FROM vysion.vysion_logs AS vl
  WHERE 1=1
    --> Include timestamp index to refine search
    AND ts_index > GETDATE() - interval '30 day'
    --> Only for Seeking.com
    AND platform = 'sa'
    --> Specific eventnames forr clicking through blog links
    AND SPLIT_PART(eventname,'-',2) IN ('blog')
)
SELECT
  v.clientsessionid, v.userid, v.eventname,
  SPLIT_PART(v.eventname,'-',2) AS panel_name,
  SPLIT_PART(v.eventname,'-',3) AS blog_name,
  v.server_ts,
  v.member_level,
  CASE
    WHEN gender = 'male' AND sexual_preference = 'female' AND account_type = 'generous' THEN 'GM4F'
    WHEN gender = 'female' AND sexual_preference = 'male' AND account_type = 'attractive' THEN 'AF4M'
    ELSE 'Other'
  END AS member_type
FROM vysion_data AS v
ORDER BY server_ts DESC


