SELECT 
  eventid, userid, clientsessionid, eventname, server_ts,
  SPLIT_PART(RIGHT(utm_medium,LEN(utm_medium)-15),'"',1) AS utm_medium,
  SPLIT_PART(RIGHT(utm_campaign,LEN(utm_campaign)-17),'"',1) AS utm_campaign,
  purchase_amt,
  MAX(CASE WHEN pt.id IS NOT NULL THEN 1 ELSE 0 END) = 1 AS saved_card_on_file,
  userdata, devicedata, eventdata, n_session_events
FROM (
  SELECT 
    eventid, userid, clientsessionid, eventname, server_ts, userdata, devicedata, eventdata,
    JSON_EXTRACT_PATH_TEXT(vysion_logs.eventdata,'digi',2,true) AS utm_medium,
    JSON_EXTRACT_PATH_TEXT(vysion_logs.eventdata,'digi',5,true) AS utm_campaign,
    -- SPLIT_PART(RIGHT(JSON_EXTRACT_PATH_TEXT(vysion_logs.eventdata,'digi',5,true),LEN(JSON_EXTRACT_PATH_TEXT(vysion_logs.eventdata,'digi',5,true))-17),'"',1) AS utm_campaign_2,
    JSON_EXTRACT_PATH_TEXT(vysion_logs.eventdata,'amount',true) as purchase_amt,
    COUNT(eventid) OVER (PARTITION BY clientsessionid) AS n_session_events
  FROM vysion.vysion_logs 
  WHERE 1=1
    AND ts_index BETWEEN '2022-06-01T00:00:00.000+00:00' AND '2022-06-03T00:00:00.000+00:00' 
    AND platform = 'sa'
    --> How to net out for only email
    AND eventdata ilike '%utm%'
    --> No null fields
    AND userid <> ''
    AND utm_campaign <> ''
    --> VWO events clutter up this analysis
    AND eventname NOT LIKE 'VWO%'
) AS v
LEFT JOIN seeking_arrange.profiles AS p ON p.uid = v.userid
LEFT JOIN seeking_arrange.customers AS c ON c.user_id = p.user_id
LEFT JOIN seeking_arrange.paymenttokens AS pt ON pt.customer_id = c.id
  --> Only active at time of event (not deleted)
  AND (pt.deleted_at > v.server_ts OR pt.deleted_at IS NULL)
  --> Only active at time of event (actually active)
  AND (v.server_ts BETWEEN pt.created_at AND pt.expiry_date)
WHERE 1=1
  --> Only successful purchases
  AND v.eventname = 'billingSuccess'
GROUP BY 1,2,3,4,5,6,7,8,10,11,12,13
LIMIT 100

SELECT 
  eventid, userid, clientsessionid, eventname, server_ts,
  SPLIT_PART(RIGHT(utm_medium,LEN(utm_medium)-15),'"',1) AS utm_medium,
  SPLIT_PART(RIGHT(utm_campaign,LEN(utm_campaign)-17),'"',1) AS utm_campaign,
  purchase_amt,
  MAX(CASE WHEN pt.id IS NOT NULL THEN 1 ELSE 0 END) = 1 AS saved_card_on_file,
  userdata, devicedata, eventdata, n_session_events
FROM (
  SELECT 
    eventid, userid, clientsessionid, eventname, server_ts, userdata, devicedata, eventdata,
    JSON_EXTRACT_PATH_TEXT(vysion_logs.eventdata,'digi',2,true) AS utm_medium,
    JSON_EXTRACT_PATH_TEXT(vysion_logs.eventdata,'digi',5,true) AS utm_campaign,
    -- SPLIT_PART(RIGHT(JSON_EXTRACT_PATH_TEXT(vysion_logs.eventdata,'digi',5,true),LEN(JSON_EXTRACT_PATH_TEXT(vysion_logs.eventdata,'digi',5,true))-17),'"',1) AS utm_campaign_2,
    JSON_EXTRACT_PATH_TEXT(vysion_logs.eventdata,'amount',true) as purchase_amt,
    COUNT(eventid) OVER (PARTITION BY clientsessionid) AS n_session_events
  FROM vysion.vysion_logs 
  WHERE 1=1
    AND ts_index BETWEEN '2022-06-01T00:00:00.000+00:00' AND '2022-06-03T00:00:00.000+00:00'
    --> Only Seeking 
    AND platform = 'sa'
    --> How to net out for only email
    AND eventdata ilike '%utm%'
    --> No null fields
    AND userid <> ''
    AND utm_campaign <> ''
    --> VWO events clutter up this analysis
    AND eventname NOT LIKE 'VWO%'
) AS v
LEFT JOIN seeking_arrange.profiles AS p ON p.uid = v.userid
LEFT JOIN seeking_arrange.customers AS c ON c.user_id = p.user_id
LEFT JOIN seeking_arrange.paymenttokens AS pt ON pt.customer_id = c.id
  --> Only active at time of event (not deleted)
  AND (pt.deleted_at > v.server_ts OR pt.deleted_at IS NULL)
  --> Only active at time of event (actually active)
  AND (v.server_ts BETWEEN pt.created_at AND pt.expiry_date)
WHERE 1=1
  --> Only successful purchases
  AND v.eventname = 'billingSuccess'
GROUP BY 1,2,3,4,5,6,7,8,10,11,12,13
LIMIT 100 

