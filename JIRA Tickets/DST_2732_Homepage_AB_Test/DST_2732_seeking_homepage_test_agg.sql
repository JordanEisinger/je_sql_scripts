WITH join_page_eventnames AS (
  SELECT 'join' AS eventname UNION
  SELECT 'wp-joinfree' AS eventname UNION
  SELECT 'wp-joinfreenow' AS eventname UNION
  SELECT 'wp-joinus' AS eventname UNION
  SELECT 'wp-joinfreetoday' AS eventname UNION
  SELECT 'wp-joinnow' AS eventname UNION
  SELECT 'wp-home_en-belowFold-benefitsS-join' AS eventname UNION
  SELECT 'wp-home_en-belowFold-benefits-join' AS eventname UNION
  SELECT 'wp-homeen-belowFold-benefits-join' AS eventname UNION
  SELECT 'wp-home_en-belowFold-benefitsA-join' AS eventname UNION
  SELECT 'wp-homeen-belowFold-benefitsS-join' AS eventname UNION
  SELECT 'wp-home-marketingNavBar-joinNow' AS eventname UNION
  SELECT 'wp-homeen-belowFold-benefitsA-join' AS eventname UNION
  SELECT 'wp-home_en-aboveFold-join' AS eventname UNION
  SELECT 'wp-home_en-belowFold-mission-join' AS eventname UNION
  SELECT 'wp-home_en-belowFold-mindset-join' AS eventname UNION
  SELECT 'wp-home-belowFold-join' AS eventname UNION
  SELECT 'wp-homeen-marketingNavBar-joinNow' AS eventname UNION
  SELECT 'wp-home-aboveFold-join' AS eventname UNION
  SELECT 'wp-homeen-aboveFold-join' AS eventname UNION
  SELECT 'wp-homeen-belowFold-mission-join' AS eventname UNION
  SELECT 'wp-home_en-marketingNavBar-joinNow' AS eventname UNION
  SELECT 'wp-homeen-belowFold-mindset-join' AS eventname UNION
  SELECT 'wp-whatdatinguplookslike' AS eventname UNION
  SELECT 'wp-es/loquedatingupes' AS eventname UNION
  SELECT 'wp-fr/aquoiressembleunerencontre' AS eventname UNION
  SELECT 'wp-de/wiehochhinausdatenfunktioniert' AS eventname UNION
  SELECT 'wp-zh/%e7%ba%a6%e4%bc%9a%e6%98%af%e4%bb%80%e4%b9%88%e6%a0%b7%e7%9a%84' AS eventname UNION
  SELECT 'wp-nl/whatdatinguplookslike' AS eventname UNION
  SELECT 'wp-pt/whatdatinguplookslike' AS eventname UNION
  SELECT 'wp-ja/datingup%e3%81%a8%e3%81%af' AS eventname UNION
  SELECT 'wp-catchvibesorfeelings' AS eventname UNION
  SELECT 'wp-es/captaondasosentimientos' AS eventname UNION
  SELECT 'wp-fr/vibrezetlaissezallervossentiments' AS eventname UNION
  SELECT 'wp-de/fangensievibesodergefuhleein' AS eventname UNION
  SELECT 'wp-zh/%e6%8d%95%e6%8d%89%e5%85%b1%e9%b8%a3%e6%88%96%e6%84%9f%e8%a7%89' AS eventname UNION
  SELECT 'wp-nl/catchvibesorfeelings' AS eventname UNION
  SELECT 'wp-pt/catchvibesorfeelings' AS eventname UNION
  SELECT 'wp-ja/%e9%9b%b0%e5%9b%b2%e6%b0%97%e3%82%84%e6%84%9f%e6%83%85%e3%82%92%e3%82%ad%e3%83%a3%e3%83%83%e3%83%81' AS eventname UNION
  SELECT 'wp-datingversusseeking' AS eventname UNION
  SELECT 'wp-es/citasversusseeking' AS eventname UNION
  SELECT 'wp-fr/rencontreversusseeking' AS eventname UNION
  SELECT 'wp-de/datingversusseeking' AS eventname UNION
  SELECT 'wp-zh/%e7%ba%a6%e4%bc%9a%e4%b8%8e%e5%af%bb%e6%b1%82' AS eventname UNION
  SELECT 'wp-nl/datingversusseeking' AS eventname UNION
  SELECT 'wp-pt/datingversusseeking' AS eventname UNION
  SELECT 'wp-ja/%e3%83%87%e3%83%bc%e3%83%88%e5%af%beseeking' AS eventname UNION
  SELECT 'wp-datingvsseeking' AS eventname UNION
  SELECT 'wp-es/citas-vs-seeking' AS eventname UNION
  SELECT 'wp-fr/rencontre-vs-seeking' AS eventname UNION
  SELECT 'wp-de/dating-vs-seeking' AS eventname UNION
  SELECT 'wp-zh/%e7%ba%a6%e4%bc%9a-%e5%af%bb%e6%b1%82' AS eventname UNION
  SELECT 'wp-nl/dating-vs-seeking' AS eventname UNION
  SELECT 'wp-pt/dating-vs-seeking' AS eventname UNION
  SELECT 'wp-ja/%e3%83%87%e3%83%bc%e3%83%88%e5%af%be-seeking' AS eventname 
)
, homepage_or_join_sessions AS (
  SELECT 
    DISTINCT clientsessionid
  FROM vysion.vysion_logs
  WHERE 1=1
    --> Test started 04/03/2023 ~12:00PM PST
    AND ts_index >= '2023-04-03T00:00:00.000+00:00'
    --> Test only on Seeking homepage
    AND platform = 'sa'
    --> Only sessions where the user either started or eventually hit the homepage (control or variant)
    --> Also includes if they hit the 'join' page during their session 
    AND (
      eventname IN (SELECT eventname FROM join_page_eventnames)
      OR
      eventname LIKE 'wp%'
    )
    --> Don't include marketing 
    AND eventname NOT ILIKE '%marketing%'
    --> No SA App users, only interested in landing pages
    AND JSON_EXTRACT_PATH_TEXT(eventdata, 'site', true) <> 'SA-App'
    -- Not user authenticated
    AND JSON_EXTRACT_PATH_TEXT(eventdata, 'userAuthenticated', true) <> 'yes'
    -- Just view events
    AND JSON_EXTRACT_PATH_TEXT(eventdata, 'action', true) = 'view'
)
--> Aggregation statement
SELECT
--   DATE_TRUNC('day',first_server_ts) AS date,
  CASE 
    WHEN landing_page_variation IS NOT NULL THEN landing_page_variation 
    WHEN landing_page_variation IS NULL AND landing_page ILIKE 'wp-home%' THEN 'wp-home' 
  END AS variants,
  COUNT(DISTINCT clientsessionid) AS distinct_sessions,
  SUM(successfully_joined) AS total_joins,
  (CAST(total_joins AS numeric) / CAST(distinct_sessions AS numeric)) * 100 AS conversion
FROM (
    SELECT
    v.first_server_ts,
    v.clientsessionid,
    COALESCE(v.userid, '') AS userid,
    GREATEST(JSON_EXTRACT_PATH_TEXT(eventdata, 'url', true), JSON_EXTRACT_PATH_TEXT(eventdata, 'view', true)) AS url,
    v.landing_page,
    v.landing_page_variation,
    v.join_page,
    v.clicked_anything_in_join_flow,
    v.attempted_captcha,
    v.attempted_to_join,
    v.successfully_joined,
    v.n_events
  FROM (
    SELECT DISTINCT 
      FIRST_VALUE(v.server_ts) OVER 
        (PARTITION BY v.clientsessionid, v.filled_userid ORDER BY v.server_ts ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) 
      AS first_server_ts,
      v.clientsessionid,
      v.filled_userid AS userid,
      FIRST_VALUE(v.eventdata IGNORE NULLS) OVER 
        (PARTITION BY v.clientsessionid, v.filled_userid ORDER BY v.server_ts ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) 
      AS eventdata,
      FIRST_VALUE(v.devicedata IGNORE NULLS) OVER 
        (PARTITION BY v.clientsessionid, v.filled_userid ORDER BY v.server_ts ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) 
      AS devicedata,
      FIRST_VALUE((CASE WHEN v.eventname LIKE 'wp%' THEN v.eventname END) IGNORE NULLS) OVER 
        (PARTITION BY v.clientsessionid, v.filled_userid ORDER BY v.server_ts ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) 
      AS landing_page,
      FIRST_VALUE(CASE WHEN v.eventname IN ('wp-homeen','wp-home_en') THEN v.eventname END IGNORE NULLS) OVER
        (PARTITION BY v.clientsessionid, v.filled_userid ORDER BY v.server_ts ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
      AS landing_page_variation,
      FIRST_VALUE(CASE WHEN (jpe.eventname <> '' OR v.eventname LIKE 'wp-%join_%') AND v.eventname NOT ILIKE '%marketing%' AND JSON_EXTRACT_PATH_TEXT(eventdata, 'action', true) = 'view' THEN v.eventname END IGNORE NULLS) OVER 
        (PARTITION BY v.clientsessionid, v.filled_userid ORDER BY v.server_ts ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) 
      AS join_page,
      FIRST_VALUE(CASE WHEN (v.eventname ILIKE '%-iam%-%' OR v.eventname ILIKE '%-email%' OR v.eventname ILIKE '%interested%') THEN v.eventname END IGNORE NULLS) OVER 
        (PARTITION BY v.clientsessionid, v.filled_userid ORDER BY v.server_ts ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) 
      AS clicked_anything_in_join_flow,
      FIRST_VALUE(CASE WHEN v.eventname ILIKE '%captcha%' THEN v.eventname END IGNORE NULLS) OVER 
        (PARTITION BY v.clientsessionid, v.filled_userid ORDER BY v.server_ts ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) 
      AS attempted_captcha,
      MAX(CASE WHEN (v.eventname = 'join-continue' OR v.eventname LIKE 'wp%-createAccount') THEN 1 ELSE 0 END) OVER 
        (PARTITION BY v.clientsessionid, v.filled_userid) 
      AS attempted_to_join,
      MAX(CASE WHEN v.filled_userid <> '' THEN 1 ELSE 0 END) OVER 
        (PARTITION BY v.clientsessionid, v.filled_userid)
      AS successfully_joined,
      CASE WHEN attempted_to_join = 0 AND successfully_joined = 1 THEN 1 ELSE 0 END AS not_a_join_session,
      COUNT(v.server_ts) OVER (PARTITION BY v.clientsessionid, v.filled_userid) AS n_events
    FROM (
      SELECT
        server_ts,
        clientsessionid,
        eventname,
        FIRST_VALUE(NULLIF(userid, '') IGNORE NULLS) OVER 
          (PARTITION BY clientsessionid ORDER BY server_ts ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) 
        AS filled_userid,
        eventdata,
        devicedata
      FROM vysion.vysion_logs 
      WHERE 1=1
        --> Test started 04/03/2023 ~12:00PM PST
        AND ts_index >= '2023-04-03T00:00:00.000+00:00'
        --> Test only for Seeking homepage 
        AND platform = 'sa'
        AND clientsessionid IN (SELECT clientsessionid FROM homepage_or_join_sessions)
    ) AS v
    LEFT JOIN join_page_eventnames AS jpe ON jpe.eventname = v.eventname
  ) AS v
  WHERE 1=1
    --> Does not include already joined users
    AND v.not_a_join_session = 0
    AND landing_page ILIKE 'wp-home%'
)
GROUP BY 1
