/* ===================================================================================
As of: 02/21/2023

General Notes:
- To find who initiated a conversation, must use messages table
    - profile_id_1 and profile_id_2 have NOTHING to do with which profile messages first
    - If a user's profile_id is lower, they are put into profile_id_1 (and vice versa)
- Use thread created_at = first message created_at to see who initiated conversation
    - Found example where first message created_at doesn't equal thread created_at (off by 1 second)
    - See if above example is widespread or only a few examples
- Instead of using above logic (many caveats), better to look at all threads in last N days
- Include count of how many are system messages (i.e. 'Request to access private photos')
- Include proprotions for better analysis

Changelog:
02/15/2023: Wrote query to count total uploaded photos
02/21/2023: Created new queery based on threads in last 7 days
=================================================================================== */

/* New Query - Gives better story, does not limit GM users*/
SELECT
  recipient_photo_count AS af_photo_count,
  COUNT(DISTINCT recipient_profile_id) AS af_profile_count,
  COUNT(thread_id) AS total_threads_started_by_gm,
  SUM(is_system) AS total_threads_started_by_system_message
FROM (
SELECT
  t.*,
  COUNT(ph.id) AS recipient_photo_count
FROM (
    SELECT
      t.id AS thread_id, 
      m.id AS message_id,
      p.id AS sender_profile_id,
      p.account_type AS sender_account_type,
      p.sex AS sender_sex,
      CASE WHEN t.profile_id_1 = p.id THEN t.profile_id_2 ELSE t.profile_id_1 END AS recipient_profile_id,
      m.is_system,
      ROW_NUMBER() OVER (PARTITION BY t.id ORDER BY m.id) AS row_num_asc
    FROM seeking_arrange.messages AS m
    INNER JOIN seeking_arrange.threads AS t ON t.id = m.conversation_id
    INNER JOIN seeking_arrange.profiles AS p ON p.id = m.profile_id
    WHERE 1=1 
      AND t.created_at >= GETDATE() - INTERVAL '7 day'
      -- AND t.created_at >= GETDATE() - INTERVAL '28 day'
      -- AND t.created_at >= GETDATE() - INTERVAL '90 day'
) AS t
LEFT JOIN seeking_arrange.approvalphotos AS ph ON ph.profile_id = t.recipient_profile_id AND ph.moderated_status = 1
WHERE 1=1
  AND row_num_asc = 1
  AND sender_account_type = 'generous'
  AND sender_sex = 'Male'
GROUP BY 1,2,3,4,5,6,7,8
)
GROUP BY 1


/* Original query - Has some problems with limiting GM users, not giving full story with results*/
WITH af_approved_photos AS (
--> AF who joined in last 7 days with total approved photos uploaded to profile (includes deleted)
SELECT 
  ph.profile_id,
  CASE
    WHEN p.account_type = 'attractive' AND p.sex = 'Female' THEN 'AF'
    WHEN p.account_type = 'generous' AND p.sex = 'Male' THEN 'GM'
    ELSE 'Other'
  END as member_type,
  COUNT(ph.id) AS total_photo_count,
  p.created_at AS join_date,
  MIN(ph.created_at) AS min_photo_created_at,
  MAX(ph.created_at) AS max_photo_created_at
FROM seeking_arrange.approvalphotos AS ph
LEFT JOIN seeking_arrange.profiles AS p ON p.id = ph.profile_id
WHERE 1=1
  -- Joined in last n days
  AND p.created_at >= GETDATE() - 7
  -- Only attractive females
  AND member_type = 'AF'
  -- Only approved photos
  AND ph.moderated_status = 1
GROUP BY 1,2,4
ORDER BY 3 DESC
),
--> Sample of GM4F accounts
gm_sample AS (
  SELECT 
    p.id,
    p.created_at,
    CASE
      WHEN p.account_type = 'attractive' AND p.sex = 'Female' THEN 'AF'
      WHEN p.account_type = 'generous' AND p.sex = 'Male' THEN 'GM'
      ELSE 'Other'
    END AS member_type
  FROM seeking_arrange.profiles AS p
  WHERE 1=1
    -- Joined in last n days
    AND p.created_at >= GETDATE() - 7
    -- Only generous males
    AND member_type = 'GM'
),
gm_messages AS (
--> Determines if a message sent by a GM was the first message in a thread (Note: Does NOT include 'requesting access to private photos')
SELECT
  m.profile_id AS gm_profile_id, 
  m.conversation_id,
  CASE
    WHEN t.profile_id_1 = gm_profile_id THEN t.profile_id_2 ELSE t.profile_id_1
  END AS messaged_profile_id,
  MIN(m.created_at) AS first_thread_message_created_at,
  t.created_at AS thread_created_at,
  CASE
    WHEN first_thread_message_created_at = thread_created_at THEN 1 ELSE 0
  END AS gm_messaged_first,
  CASE
    WHEN first_thread_message_created_at <> thread_created_at THEN 1 ELSE 0
  END AS af_messaged_first
FROM seeking_arrange.messages AS m
INNER JOIN gm_sample ON gm_sample.id = m.profile_id
LEFT JOIN seeking_arrange.threads AS t ON t.id = m.conversation_id
GROUP BY 1,2,3,5
ORDER BY m.profile_id DESC, conversation_id DESC, first_thread_message_created_at ASC
)
--> Aggregated
SELECT
  a.total_photo_count AS af_total_photos,
  SUM(gm_messaged_first) AS total_threads_started_by_gm,
  SUM(af_messaged_first) AS total_threads_started_by_af
FROM gm_messages AS gm
INNER JOIN af_approved_photos AS a ON a.profile_id = gm.messaged_profile_id
GROUP BY 1
ORDER BY 1 ASC

--> At the individual conversation level
SELECT 
  gm.gm_profile_id,
  gm.conversation_id,
  gm.messaged_profile_id,
  gm.gm_messaged_first,
  gm.af_messaged_first,
  a.total_photo_count AS af_total_photos
FROM gm_messages AS gm
INNER JOIN af_approved_photos AS a ON a.profile_id = gm.messaged_profile_id
WHERE 1=1
  AND af_total_photos = 13
ORDER BY gm_profile_id DESC, conversation_id DESC 