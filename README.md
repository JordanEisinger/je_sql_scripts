# Jordan E SQL Scripts

* *Author:* Jordan Eisinger, Data Scientist  
* *Description*: Repo for Reflex Media SQL scripts, mostly for JIRA tickets

## Folder Contents

* **JIRA**: All SQL scripts related to open, in progress, and closed JIRA tickets
* **Other Queries**: Repeatedly used ad hoc queries that aren't tied to one specific ticket (includes Legal Requests)
* **Payment Reconciliation**: SQL scripts for payment reconcilliatoin project.
