WITH hbms_planet_transaction_mapping AS (
SELECT
  hbms.id,
  hbms.rrn,
  hbms.transaction_type,
  hbms.clearing_currency,
  hbms.clearing_amount,
  hbms.acquirer_trans_date,
  hbms.settlement_date,
  i.po_id AS inovio_po_id,
  i.trans_amount,
  i.currency_settled,
  i.trans_settle_utc_ts,
  p.id AS u2p_payment_id,
  p.amount,
  p.currency,
  p.status,
  p.created_at AS u2p_created_at,
  (getdate() || '+00:00')::timestamptz as created_at
FROM payment.hbms_planet_transactions AS hbms
LEFT JOIN analytics.inovio_order_detail AS i ON i.proc_retrieval_num = hbms.rrn
LEFT JOIN u2p_sites.payments AS p ON p.transaction_reference = i.po_id AND p.transaction_reference <> ''
WHERE 1=1
  -- Null entries for every field
  AND hbms.id <> 'd41d8cd98f00b204e9800998ecf8427e'
  -- 13 transactions with multiple records each
  -- AND hbms.rrn NOT IN
  --   ('108306796100','113939633438','126113876246','114342143661','117459867698','122591116363','117459869937','117459808891','117459844630',
  --     '113034728727','113034727382','117459860837','126315005356')
)

SELECT
  m.u2p_payment_id,
  pl.u2p_paymentlogs_transaction_references,
  p.currency AS u2p_payment_currency,
  p.status AS u2p_payment_status,
  SUM(h.clearing_amount) AS hbms_total_transaction_amount,
  pl.total_u2p_paymentlogs_amount,
  i.total_inovio_trans_amount,
  s.name AS site_name,
  p.created_at::timestamp AS u2p_payment_created_at,
  MIN(h.acquirer_trans_date)::timestamp AS min_hbms_transaction_date,
  MAX(h.acquirer_trans_date)::timestamp AS max_hbms_transaction_date,
  i.min_trans_auth_utc_ts::timestamp AS min_inovio_trans_auth_utc_ts,
  i.max_trans_auth_utc_ts::timestamp AS max_inovio_trans_auth_utc_ts,
  LISTAGG(DISTINCT m.id, ',') AS hbms_ids,
  ABS(hbms_total_transaction_amount - GREATEST(COALESCE(total_inovio_trans_amount, 0))) AS hbms_vs_gateway_discrepancy,
  ABS(hbms_total_transaction_amount - total_u2p_paymentlogs_amount) AS hbms_vs_u2p_discrepancy,
  LEAST(u2p_payment_created_at, min_hbms_transaction_date, min_inovio_trans_auth_utc_ts) AS min_transaction_ts,
  GREATEST(u2p_payment_created_at, max_hbms_transaction_date, max_inovio_trans_auth_utc_ts) AS max_transaction_ts

FROM payment.hbms_planet_transactions AS h
LEFT JOIN hbms_planet_transaction_mapping AS m ON m.id = h.id
LEFT JOIN u2p_sites.payments AS p ON p.id = m.u2p_payment_id
-- Get Site names:
LEFT JOIN u2p_core.sites AS s ON s.id = p.site_id

--> Get U2P paymentlog summed amount ->
LEFT JOIN (
  SELECT
    payment_id,
    SUM(amount) AS total_u2p_paymentlogs_amount,
    LISTAGG(DISTINCT transaction_reference, ',') AS u2p_paymentlogs_transaction_references
  FROM u2p_sites.paymentlogs
  WHERE 1=1
    AND status <> 'pending'
  GROUP BY payment_id
) AS pl ON pl.payment_id = p.id

--> Get inoviopay amounts ->
LEFT JOIN (
  SELECT
    po_id,
    proc_retrieval_num,
    MIN(trans_auth_utc_ts) AS min_trans_auth_utc_ts,
    MAX(trans_auth_utc_ts) AS max_trans_auth_utc_ts,
    SUM(trans_amount) AS total_inovio_trans_amount
  FROM analytics.inovio_order_detail
  WHERE 1=1
    AND trans_status = 'APPROVED' -- If DECLINED, transaction was not settled
-- AND trans_settle_utc_ts IS NOT NULL -- If NULL, transaction was not settled
    AND prod_name <> 'Test Mark' -- Test Payments
  GROUP BY 1,2
) AS i ON i.proc_retrieval_num = m.rrn

WHERE 1=1
  -- Only payments from U2P where transaction data exists from EPX:
--   AND p.created_at >= (SELECT MIN(h.acquirer_trans_date) FROM payment.hbms_epx_transactions)
GROUP BY 1,2,3,4,6,7,8,9,12,13;;