WITH inputs AS (
SELECT
  'INSERT HERE' AS id
)
SELECT * FROM payment.cw_trisource_transactions WHERE tranid = (SELECT id FROM inputs)
SELECT * FROM analytics.rocketgate_txn_detail WHERE tr_retrievalno = (SELECT id FROM inputs)
SELECT * FROM analytics.inovio_order_detail WHERE proc_retrieval_num = (SELECT id FROM inputs)