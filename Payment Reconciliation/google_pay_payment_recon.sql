/*
  Get explicit information regarding Google Pay payments
  joined with U2P Payments, SA Payments, and SA Mobile Subscription Logs.
  
  Only receives the correct currency and amounts for INITIAL_RECURRING and SUBSEQUENT_RECURRING category SA Payments.
  Correct currency and amount information for ONE_TIME payments can be found in seeking_arrange.payments.extended_attributes
  OR in u2p_sites.payments.request_data (both encrypted)
*/
SELECT
  order_number AS google_order_number,
  currency_of_sale AS google_currency,
  SUM(item_price) as google_total_item_price,
  SUM(charged_amount) as total_charged_amount,
  gp.order_charged_date AS google_order_charged_date,
  t.u2p_payment_id,
  t.transaction_reference, 
  t.currency as pl_currency,
  pl_amount,
  v.sa_payment_id,
  v.sa_payment_category,
  v.sa_currency,
  v.sa_amount,
  v.merchant_order_id AS msl_merchant_order_id,
  v.currency as msl_currency,
  ms_amount
FROM payment.googleplay gp
LEFT JOIN (
  -- get U2P payments table data and sum of paymentlogs.amount column
  SELECT 
    p.id AS u2p_payment_id,
    p.transaction_reference,
    pl.currency,
    sum(pl.amount) as pl_amount
  FROM u2p_sites.payments p
  LEFT JOIN u2p_sites.paymentlogs pl on p.id = pl.payment_id
  WHERE 1=1
    AND p.transaction_reference ilike 'gpa%'
    AND p.status NOT IN ('declined', 'error')
  GROUP BY 1, 2, 3
) AS t on t.transaction_reference = gp.order_number

LEFT JOIN(
  -- get SA payments table data and supplement with grouped data from mobilesubscriptionlogs
  -- Only gets the info we need for SA payments where category <> 'ONE_TIME'
  SELECT
    p.id AS sa_payment_id,
    p.transaction_seq,
    p.amount AS sa_amount,
    p.currency AS sa_currency,
    p.category AS sa_payment_category,
    ms.merchant_order_id,
    ms.currency,
    ms.amount as ms_amount
  FROM seeking_arrange.payments p
  
  LEFT JOIN (
    /*
      There are multiple rows for each merchant_order_id
      We only need the amount and currency for each distinct merchant_order_id (transaction_reference / google pay order_number)
    Hence we group
    */
    SELECT 
      merchant_order_id, 
      amount, 
      currency 
    FROM seeking_arrange.mobilesubscriptionlogs 
    GROUP BY 1,2,3
) ms on p.transaction_reference = ms.merchant_order_id
  
  WHERE 1=1
	  AND p.status NOT IN ('DECLINED', 'ERROR')
	  AND p.gateway_processor = 'GooglePay'
	  
) AS v ON v.transaction_seq = t.u2p_payment_id
WHERE 1=1
  AND gp.order_charged_date >= '2021-01-01'
  AND gp.currency_of_sale <> 'USD'
  -- AND google_order_number = 'GPA.3399-9682-8952-19693'
GROUP BY 1, 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
LIMIT 500
