-- 353,466 total ordernumbers, 282,454 distinct ordernumbers
SELECT COUNT(DISTINCT ordernumber) FROM payment.cw_trisource_transactions WHERE ordernumber <> ''

-- CW records with ordernumber: 282,454 (# of distinct ordernumbers)
SELECT
  COUNT(ordernumber)
FROM(
SELECT
  cw.ordernumber,
  SUM(cw.totalauthamount) AS trans_amount, 
  MAX(transaction_ts) AS trans_date,
  p.id AS payment_id,
  p.transaction_reference,
  p.gateway_id,
  p.site_id,
  p.amount,
  p.created_at,
  rg.tr_id,
  rg.tr_amount,
  i.po_id,
  SUM(i.trans_amount) as trans_amount
FROM payment.cw_trisource_transactions AS cw
LEFT JOIN u2p_sites.payments AS p on p.transaction_reference = cw.ordernumber
LEFT JOIN analytics.rocketgate_txn_detail AS rg ON rg.tr_id = cw.ordernumber
LEFT JOIN analytics.inovio_order_detail AS i ON i.po_id = cw.ordernumber AND i.trans_status = 'APPROVED'
WHERE 1=1
  AND cw.ordernumber <> ''
GROUP BY 1,4,5,6,7,8,9,10,11,12
ORDER BY trans_date DESC
LIMIT 500
)

-- 60,879 total records, 48,708 distinct records on tranid
SELECT COUNT(DISTINCT tranid) FROM payment.cw_trisource_transactions WHERE ordernumber = ''

-- CW records without ordernumber from RocketGate: 6,875
SELECT
  COUNT(tranid)
FROM(
SELECT
  cw.tranid,
  SUM(cw.totalauthamount) AS trans_amount, 
  MAX(transaction_ts) AS trans_date,
  rg.tr_retrievalno,
  rg.tr_amount
FROM payment.cw_trisource_transactions AS cw
INNER JOIN analytics.rocketgate_txn_detail AS rg ON cw.tranid = rg.tr_retrievalno
  AND rg.tr_retrievalno <> ''
WHERE 1=1
  AND cw.ordernumber = ''
GROUP BY cw.tranid, rg.tr_retrievalno, rg.tr_amount
-- LIMIT 500
)

-- CW records without ordernumber from InovioPay: 62
SELECT
  COUNT(tranid)
FROM(
SELECT
  cw.tranid,
  SUM(cw.totalauthamount) AS trans_amount, 
  MAX(transaction_ts) AS trans_date,
  i.proc_retrieval_num,
  SUM(i.trans_amount) as trans_amount
FROM payment.cw_trisource_transactions AS cw
INNER JOIN analytics.inovio_order_detail AS i ON i.proc_retrieval_num = cw.tranid
  AND i.proc_retrieval_num <> ''
WHERE 1=1
  AND cw.ordernumber = ''
GROUP BY cw.tranid, i.proc_retrieval_num, i.merch_acct_name
-- LIMIT 500
)

-- 60,879 total records, 6,633 distinct records on banknet
SELECT COUNT(DISTINCT banknet) FROM payment.cw_trisource_transactions WHERE ordernumber = ''

-- CW records without ordernumber from RocketGate: 211
SELECT
  COUNT(banknet)
FROM(
SELECT
  cw.banknet,
  SUM(cw.totalauthamount) AS trans_amount, 
  MAX(transaction_ts) AS trans_date,
  rg.tr_retrievalno,
  rg.tr_amount
FROM payment.cw_trisource_transactions AS cw
INNER JOIN analytics.rocketgate_txn_detail AS rg ON cw.banknet = rg.tr_retrievalno
  AND rg.tr_retrievalno <> ''
GROUP BY cw.banknet, rg.tr_retrievalno, rg.tr_amount
-- LIMIT 500
)

-- CW records without ordernumber from InovioPay: 6,314
SELECT
  COUNT(banknet)
FROM(
SELECT
  cw.banknet,
  SUM(cw.totalauthamount) AS trans_amount, 
  MAX(transaction_ts) AS trans_date,
  i.proc_retrieval_num,
  SUM(i.trans_amount) as trans_amount
FROM payment.cw_trisource_transactions AS cw
INNER JOIN analytics.inovio_order_detail AS i ON i.proc_retrieval_num = cw.banknet
  AND i.proc_retrieval_num <> ''
WHERE 1=1
  AND cw.ordernumber = ''
GROUP BY cw.banknet, i.proc_retrieval_num, i.merch_acct_name
-- LIMIT 500
)

